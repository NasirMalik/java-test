package com.nasir.java8.test2;

/**
 * Created by nasir on 20/5/19.
 */
public class StaticTest {

    public static void main(String[] args) {
//        Static1 static1 = new Static1();
//        System.out.println(static1.var1);
//        System.out.println(static1.getInst_var1());
//
//        static1 = new Static2();
//        System.out.println(static1.var1);
//        System.out.println(static1.inst_var1);
//
//        System.out.println(static1.getInst_var1());
//
//        System.out.println(((Static2)static1).var1);
//        System.out.println(((Static2)static1).var2);
//        System.out.println(((Static2)static1).var3);
//
//        Static2 static2 = new Static2();
//        System.out.println(static2.var1);



        HotBeverage.HotTea ht = new HotBeverage.HotTea();
        //call the method we made
        ht.displayOutput();

//        HotBeverage hb = new HotBeverage();
        HotBeverage.HotCoffee hc = (new HotBeverage()).new HotCoffee();
        //call the method we made
        hc.displayOutput();
    }

}

class Static1 {
    public static String var1 = "Testing Static 1";
    public static String var2 = "Testing Static 2";

    public String inst_var1 = "Testing Instance Var 1";

    public String getInst_var1() {
        System.out.println(var1);
        return "from getInst_var1 1";
    }
}

class Static2 extends Static1 {
    public static String var1 = "Testing Static 2 1";
    public static String var2 = "Testing Static 2 2";

    public String inst_var1 = "Testing Instance Var 2 1";

    public String getInst_var1() {
        System.out.println(var1);
        return "from getInst_var1 2 1";
    }
}

class HotBeverage {
    private static String note = "Hot Beverage";
    public static class HotTea {
        public void displayOutput() {
            System.out.println("Hot Tea says: " + note);
        }
    }

    public class HotCoffee {
        public void displayOutput() {
            System.out.println("Hot Coffee says: " + note);
        }
    }
}
