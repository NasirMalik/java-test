package com.nasir.java8.test2;

/**
 * Created by nasir on 17/5/19.
 */
public class LowLevel {

    public static void main(String[] args) throws ClassNotFoundException, InstantiationException, InterruptedException, IllegalAccessException {
        // testing Class.forName
//        new LowLevel().testTest();

        // Testing method with class name
//        new Test().Test("Testing method with class name");

        // Testing overloaded constructor
        new Test("Testing overloaded constructor").Test("Testing method with class name");
    }

    public void testTest() throws ClassNotFoundException, IllegalAccessException, InstantiationException, InterruptedException {
        System.out.println("Testing");
        Test test = (Test) (Class.forName("com.nasir.java8.test2.Path")).newInstance();
//        Test test = new Path();
        test.testMessage("From new Invocation");
    }

}

class Test {

    private Test() { System.out.println("Inside noarg constructor"); }

    public Test(String message) {
        this();
        System.out.println(message);
    }

    public void Test(String message) {
        System.out.println(message);
    }

    public void testMessage(String message) {
        System.out.println(message);
    }
}
