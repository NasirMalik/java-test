package com.nasir.java8.test2;

import java.util.Arrays;
import java.util.List;

/**
 * Created by nasir on 27/5/19.
 */
public class WildcardTest {

    public static void main(String[] args)
    {

        //Upper Bounded Integer List
        List<Integer> list1= Arrays.asList(4,5,6,7);
        //printing the sum of elements in list
        System.out.println("Total sum is:" + relaxedSum(list1));
        print(list1);

        //Double list
        List<Double> list2=Arrays.asList(4.1,5.1,6.1);
        //printing the sum of elements in list
        System.out.println("Total sum is : " + relaxedSum(list2));
        print(list2);

        //Upper Bounded Integer List
        List<Number> list3= Arrays.asList(4,5,6,7);

        //printing the sum of elements in list
        System.out.println("Total sum is:" + strictSum(list3));

        //Double list
        List<Number> list4=Arrays.asList(4.1,5.1,6.1);

        //printing the sum of elements in list
        System.out.println("Total sum is : " + strictSum(list4));
    }

    private static double relaxedSum(List<? extends Number> list)
    {
        double sum = 0.0;
        for (Number i : list)
        {
            sum += i.doubleValue();
        }
        return sum;
    }

    private static void print(List list)
    {
        System.out.println("Total list is : " + list);
    }

    private static double strictSum(List<Number> list)
    {
        double sum = 0.0;
        for (Number i : list)
        {
            sum += i.doubleValue();
        }
        return sum;
    }

}

abstract class Base {
    abstract void method1();
    abstract void method2();
}

class Derived1 extends Base {
    void method1(){ System.out.println("From method1() in Derived1"); }
    void method2(){ System.out.println("From method2() in Derived1"); }
}

class Derived2 extends Base {
    void method1(){ System.out.println("From method1() in Derived2"); }
    void method2(){ System.out.println("From method2() in Derived2"); }
}