package com.nasir.java8.test2;

/**
 * Created by nasir on 20/5/19.
 */
public class VarargsTest {

    public static void main(String... args) {
        System.out.printf("Testing the C Format Style %1s %2d", "ABCD", 234);
        testVaragrs("ABCD");
    }

    static void testVaragrs(String arg1, String ...args) {
        System.out.println("\nTesting the Varargs " +  arg1);
        for (String arg : args) {
            System.out.printf("Testing the Varargs %1s", arg);
        }
    }

}
