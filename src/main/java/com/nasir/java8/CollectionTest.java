package com.nasir.java8;

import java.util.Arrays;
import java.util.Collection;

public class CollectionTest {

    public static void main(String[] args) {
        Product door = new Product("door", "wooden");
        Product window = new Product("window", "frame");
        Product car = new Product("car", "movable");

        Collection<Product> allProducts = Arrays.asList(door, window, car);
        Collection<Product> someProducts = Arrays.asList(door, car);

        System.out.println(allProducts);

        car.setType("brokendown");

        System.out.println(allProducts);

        changeProducts(allProducts);

        System.out.println(allProducts);

        System.out.println(car);

        System.out.println(someProducts);
    }

    private static void changeProducts(Collection<Product> products) {
        products.forEach(product -> product.setType(product.getType().toUpperCase()));
    }

}

class Product {
    private String name;
    private String type;

    public Product() {}

    public Product(String name, String type) {
        this.name = name;
        this.type = type;
    }

    @Override
    public String toString() {
        return "Product{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}