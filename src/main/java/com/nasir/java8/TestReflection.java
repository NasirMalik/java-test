package com.nasir.java8;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;

public class TestReflection {

    public static void main(String[] args) throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        Main m = new Main();

        Method[] methods = m.getClass().getMethods();
        Arrays.stream(methods).forEach(method -> {
            try {
                System.out.print("\n" + method.getName() + "\t=> ");
                method.invoke(m);
            } catch (Exception e) {
                System.out.print("\t-> " +  e.getMessage());
            }
        });

        Field[] fields = m.getClass().getFields();
        Arrays.stream(fields).forEach(field -> {
            try {
                System.out.print("\n" + String.join(", ", field.getName(), field.getGenericType().toString()));
            } catch (Exception e) {
                System.out.print("\n-> " +  e.getMessage());
            }
        });
    }

}

class Main {

    public int testInt;
    public String testStr;

    public void myPublic() {
        System.out.println("Testing by invoking from reflection");
    }

    public void myPublic2() {
        System.out.println("Testing by invoking from reflection - 2");
    }

    private void myPrivate() {
    }

    public class Nested {

        public void nestedPublic() {
            myPrivate();
        }
    }
}
