package com.nasir.java8.hRank;

import java.util.Scanner;

public class MovingProduct {

    public static void main(String []argh){
        Scanner in = new Scanner(System.in);
        int t=in.nextInt();
        int[][] queries = new int[t][3];

        for(int i=0;i<t;i++){
            queries[i][0] = in.nextInt();
            queries[i][1] = in.nextInt();
            queries[i][2] = in.nextInt();
        }

        for(int i=0;i<t;i++){
            int last = queries[i][0];
            for (int j=0; j<queries[i][2]; j++) {
                last += Math.pow(2, j) * queries[i][1];
                System.out.print(last + " ");
            }
            System.out.println();
        }

        in.close();
    }

}
