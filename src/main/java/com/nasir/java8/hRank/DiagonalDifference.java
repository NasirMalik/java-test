package com.nasir.java8.hRank;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.*;
import java.util.regex.*;
import java.util.stream.*;
import static java.util.stream.Collectors.joining;
import static java.util.stream.Collectors.toList;

public class DiagonalDifference {
    /*
     * Complete the 'diagonalDifference' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts 2D_INTEGER_ARRAY arr as parameter.
     */

    public static int diagonalDifference(List<List<Integer>> arr) {
        int[] sumArray = {0, 0};
        int size = arr.size()-1;
        for (int i = 0; i <= size; i++) {
            sumArray[0] += arr.get(i).get(i);
            sumArray[1] += arr.get(i).get(size-i);
        }
        return Math.abs(sumArray[0] - sumArray[1]);
    }

    // Complete the plusMinus function below.
    static void plusMinus(int[] arr) {
        System.out.println((double) Arrays.stream(arr).filter(num ->  Math.signum(num) == 1).count() / arr.length);
        System.out.println((double) Arrays.stream(arr).filter(num ->  Math.signum(num) == -1).count() / arr.length);
        System.out.println((double) Arrays.stream(arr).filter(num ->  Math.signum(num) == 0).count() / arr.length);
    }

    // Complete the staircase function below.
    static void staircase(int n) {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n-(i+1); j++) {
                System.out.print(" ");
            }
            for (int j = 0; j < i+1; j++) {
                System.out.print("#");
            }
            System.out.println();
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(bufferedReader.readLine().trim());

        List<List<Integer>> arr = new ArrayList<>();

        IntStream.range(0, n).forEach(i -> {
            try {
                arr.add(
                        Stream.of(bufferedReader.readLine().replaceAll("\\s+$", "").split(" "))
                                .map(Integer::parseInt)
                                .collect(toList())
                );
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });

        System.out.println(diagonalDifference(arr));

        bufferedReader.close();

    }

}

