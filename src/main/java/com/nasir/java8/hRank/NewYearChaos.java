package com.nasir.java8.hRank;

import java.util.*;

public class NewYearChaos {

    // Complete the minimumBribes function below.
    static void minimumBribes(int[] q) {
        System.out.println(Arrays.toString(q));

        int totalBribes = 0;
        int currentBribe = 0;

        for (int i=0; i<q.length; i++) {
            if (q[i] == i+1) {
                continue;
            }
            else {
                currentBribe = Math.abs(q[i] - (i+1));
                if (currentBribe > 2) {
                    System.out.println("Too chaotic");
                    return;
                }
                else {
                    totalBribes += currentBribe;
                    i += 1;
                    if (currentBribe == 1) {
                        if (Math.abs(q[i] - (i+1)) == 1) {
                            currentBribe = 0;
                            continue;
                        }
                    }
                    if (currentBribe == 2) {
                        if (Math.abs(q[i] - (i+1)) == 1 && Math.abs(q[i+1] - (i+2)) == 1) {
                            i += 1;
                            currentBribe = 0;
                            continue;
                        }
                    }
                }
            }
        }

        System.out.println(totalBribes);

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
//        int t = scanner.nextInt();
//        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//
//        for (int tItr = 0; tItr < t; tItr++) {
//            int n = scanner.nextInt();
//            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//
//            int[] q = new int[n];
//
//            String[] qItems = scanner.nextLine().split(" ");
////            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//
//            for (int i = 0; i < n; i++) {
//                int qItem = Integer.parseInt(qItems[i]);
//                q[i] = qItem;
//            }
//
//            minimumBribes(q);
//        }
//
//        scanner.close();

        int i = 0;
        for (;;)
        {
//            if (i == 3)
//            {
//                break;
//            }
            if (i == 4){
                break;
            }
            i++;
        }
        System.out.println(i);
    }
}

