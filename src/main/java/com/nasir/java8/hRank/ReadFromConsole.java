package com.nasir.java8.hRank;

import java.io.BufferedReader;
import java.io.Console;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Scanner;

public class ReadFromConsole {

    public static void main(String[] args) {
        withScanner();
//        withScanner3Ints();
//        withScanner3Types();
//        withBufferedReader();
//        withConsole();
    }

    private static void withScanner(){
        // Create a Scanner object to read input from stdin.
        Scanner scan = new Scanner(System.in);

        // Read a full line of input from stdin and save it to our variable, inputString.
        String inputString = scan.nextLine();
        String inputString2 = scan.nextLine();

        // Close the scanner object, because we've finished reading
        // all of the input from stdin needed for this challenge.
        scan.close();

        // Print a string literal saying "Hello, World." to stdout.
        System.out.println("Hello, World.");

        // TODO: Write a line of code here that prints the contents of inputString to stdout.
        System.out.println(inputString);
        System.out.println(inputString2);
    }

    private static void withScanner3Ints(){
        // Create a Scanner object to read input from stdin.
        Scanner scan = new Scanner(System.in);

        int[] intArray = new int[3];
        intArray[0] = scan.nextInt();
        intArray[1] = scan.nextInt();
        intArray[2] = scan.nextInt();

        // Close the scanner object, because we've finished reading
        // all of the input from stdin needed for this challenge.
        scan.close();

        // Print a string literal saying "Hello, World." to stdout.
        Arrays.stream(intArray).boxed().forEach(System.out::println);
    }

    private static void withScanner3Types(){
        Scanner scan = new Scanner(System.in);
        int i = scan.nextInt();
        double d = scan.nextDouble();
        String s = scan.nextLine();
        s = scan.nextLine();

        // Write your code here.

        System.out.println("String: " + s);
        System.out.println("Double: " + d);
        System.out.println("Int: " + i);
    }

    private static void withBufferedReader(){
        BufferedReader buffer = new BufferedReader(new InputStreamReader(System.in));

        // Read a full line of input from stdin and save it to our variable, inputString.
        try{
            String inputString = buffer.readLine();
            System.out.println("Hello, World.");

            // TODO: Write a line of code here that prints the contents of inputString to stdout.
            System.out.println(inputString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void withConsole(){
        Console console = System.console();
        if (console != null) {
            String inputString = console.readLine();
            System.out.println("Hello, World.");

            // TODO: Write a line of code here that prints the contents of inputString to stdout.
            System.out.println(inputString);
        }
    }

}
