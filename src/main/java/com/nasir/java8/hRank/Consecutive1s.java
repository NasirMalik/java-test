package com.nasir.java8.hRank;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Consecutive1s {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        Pattern p = Pattern.compile("1+");
        Matcher m;
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        while(scanner.hasNext()){
            int n = scanner.nextInt();

            String binary = Integer.toBinaryString(n);
            m = p.matcher(binary);

            System.out.println("Binary for " + n + " is : " + binary);
            int maxConsecutive = 0;

            while (m.find()) {
                System.out.println(m.group());
                if (maxConsecutive < m.group().length()){
                    maxConsecutive = m.group().length();
                }
            }

            System.out.println("Max consecutive 1's are : " + maxConsecutive);

        }

        scanner.close();
    }

}
