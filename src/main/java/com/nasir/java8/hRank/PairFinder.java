package com.nasir.java8.hRank;

import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class PairFinder {

    /**
     * Find Pairs of matching colors
     *
     * @param n     size of array
     * @param ar    array of colored socks
     * @return
     */
    static int sockMerchant(int n, int[] ar) {
//        return Arrays.stream(ar).boxed()
//                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
//                .values().stream()
//                .map(value -> (value/2))
//                .filter(value -> value>0)
//                .collect(Collectors.summingInt(Long::intValue));

        return Arrays.stream(ar).boxed()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .values().stream()
                .mapToInt(value -> (int)(value/2))
                .filter(value -> value>0)
                .sum();

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] ar = new int[n];

        String[] arItems = scanner.nextLine().split(" ");

        for (int i = 0; i <n; i++) {
            int arItem = Integer.parseInt(arItems[i]);
            ar[i] = arItem;
        }

        scanner.close();

        System.out.println(sockMerchant(n, ar));


    }

}
