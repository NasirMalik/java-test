package com.nasir.java8.hRank;

import java.util.Scanner;

public class EvenOdd {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int N = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String[][] lines = new String[N][2];

        for (int i=0; i<N; i++) {
            String line = scanner.nextLine();
            lines[i][0] = "";
            lines[i][1] = "";
            for (int j=0; j<line.length(); j++) {
                char thisChar = line.charAt(j);
                if (j % 2 == 0) {
                    lines[i][0] += thisChar;
                } else {
                    lines[i][1] += thisChar;
                }
            }
        }

        for (int i=0; i<N; i++) {
            System.out.printf("%s %s%n", lines[i][0], lines[i][1]);
        }

        scanner.close();
    }
}
