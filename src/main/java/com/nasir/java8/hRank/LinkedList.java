package com.nasir.java8.hRank;

import java.util.*;

class LinkedNode {
    int data;
    LinkedNode next;
    LinkedNode(int d) {
        data = d;
        next = null;
    }
}

public class LinkedList {

    public static LinkedNode insert(LinkedNode head, int data) {
        if (head == null) {
            head = new LinkedNode(data);
        }
        else {
            LinkedNode tail = head;
            while (tail.next != null){
                tail = tail.next;
            }
            tail.next = new LinkedNode(data);
        }
        return head;
    }

    public static void display(LinkedNode head) {
        LinkedNode start = head;
        while(start != null) {
            System.out.print(start.data + " ");
            start = start.next;
        }
    }

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        LinkedNode head = null;
        int N = sc.nextInt();

        while(N-- > 0) {
            int ele = sc.nextInt();
            head = insert(head,ele);

//            display(head);
        }
        display(head);
        sc.close();
    }
}