package com.nasir.java8.hRank;

import java.util.Scanner;

public class Formatting {

    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        String[] strs = new String[3];
        int[] ints = new int[3];

        for(int i=0;i<3;i++)
        {
            strs[i] = sc.next();
            ints[i] = sc.nextInt();
//            sc.nextLine();
        }

        System.out.println("================================");
        for(int i=0;i<3;i++)
        {
            System.out.printf("%n%-15s%03d", strs[i], ints[i]);
        }
        System.out.println("================================");

        new Formatting().test();

    }

    private void test(){
        try{
            Float f = new Float("3.0");
            int x = f.intValue();
            byte b = f.byteValue();
            double d = f.doubleValue();
            System.out.println(x + b + d);
        }
        catch (NumberFormatException e) {
            System.out.println("bad");
        }

        boolean b1 = true;
        boolean b2 = false;
        boolean b3 = true;
        if (b1 & b2 | b2 & b3 | b2) {
            System.out.println("ok ");
        }
        if (b1 & b2 | b2 & b3 | b2 | b1) {
            System.out.println("dokey");
        }

    }

}
