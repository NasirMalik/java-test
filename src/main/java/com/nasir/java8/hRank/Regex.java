package com.nasir.java8.hRank;

import java.util.Scanner;

public class Regex {

    public static void main(String[] args) {
        String pattern = ".+@.+\\.[0-9,A-Z,a-z]+"; //"^[\\w]{4,20}(\\.[\\w]{1,20}){0,20}@[\\w]{4,10}(\\.[\\w]{1,20}){1,2}";   // Email
//        String pattern = "([01]?\\d?\\d|2[0-4]\\d|25[0-5])" +
//                "\\.([01]?\\d?\\d|2[0-4]\\d|25[0-5])" +
//                "\\.([01]?\\d?\\d|2[0-4]\\d|25[0-5])" +
//                "\\.([01]?\\d?\\d|2[0-4]\\d|25[0-5])";                                              // IP Address

        Scanner in = new Scanner(System.in);
        while(in.hasNext()){
            String IP = in.next();
            System.out.println(IP.matches(pattern));
        }
    }

}
