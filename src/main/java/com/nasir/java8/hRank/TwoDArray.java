package com.nasir.java8.hRank;

import java.util.Scanner;

public class TwoDArray {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int maxX= 6, maxY = 6;
        int[][] arr = new int[maxX][maxY];

        for (int i = 0; i < maxX; i++) {
            String[] arrRowItems = scanner.nextLine().split(" ");
            scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

            for (int j = 0; j < maxY; j++) {
                int arrItem = Integer.parseInt(arrRowItems[j]);
                arr[i][j] = arrItem;
            }
        }

        Integer maxHourGlass = null;

        for (int i = 0; i < maxX-2; i++) {
            for (int j = 0; j < maxY-2; j++) {
                int thisHourGlass = (arr[i][j] + arr[i][j+1] + arr[i][j+2])
                        + (arr[i+1][j+1])
                        + (arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2]);
                if (maxHourGlass == null || thisHourGlass > maxHourGlass) {
                    maxHourGlass = thisHourGlass;
                    System.out.println(maxHourGlass);
                }
            }
        }

//        -1 -1 0 -9 -2 -2
//        -2 -1 -6 -8 -2 -5
//        -1 -1 -1 -2 -3 -4
//        -1 -9 -2 -4 -4 -5
//        -7 -3 -3 -2 -9 -9
//        -1 -3 -1 -2 -4 -5


        System.out.println(maxHourGlass);

        scanner.close();
    }

}
