package com.nasir.java8.hRank;

import java.util.*;

class Node{
    Node left,right;
    int data;
    Node(int data){
        this.data=data;
        left=right=null;
    }
}

public class BST {

    static void printLevelOrder(Node root, int height)
    {
        for (int i=1; i<=height+1; i++)
            printGivenLevel(root, i);
    }

    /* Print nodes at the given level */
    static void printGivenLevel (Node root ,int level)
    {
        if (root == null)
            return;
        if (level == 1)
            System.out.print(root.data + " ");
        else if (level > 1)
        {
            printGivenLevel(root.left, level-1);
            printGivenLevel(root.right, level-1);
        }
    }

    public static int getHeight(Node root){
        int leftHeight = (root.left != null ? 1 + getHeight(root.left) : 0);
        int rightHeight = (root.right != null ? 1 + getHeight(root.right) : 0);
        return leftHeight > rightHeight ? leftHeight : rightHeight;
    }

    public static Node insert(Node root, int data){
        if(root==null){
            return new Node(data);
        }
        else{
            Node cur;
            if(data<=root.data){
                cur=insert(root.left,data);
                root.left=cur;
            }
            else{
                cur=insert(root.right,data);
                root.right=cur;
            }
            return root;
        }
    }

    public static void main(String args[]){
        Scanner sc=new Scanner(System.in);
        int T=sc.nextInt();
        Node root=null;
        while(T-->0){
            int data=sc.nextInt();
            root = insert(root,data);
        }

        int height=getHeight(root);
        System.out.println(height);

        printLevelOrder(root, height);
    }
}
