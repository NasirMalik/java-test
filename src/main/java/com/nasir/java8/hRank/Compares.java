package com.nasir.java8.hRank;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Compares {

    public static void main(String[] args) {
        List<List<String>> items = new ArrayList<List<String>>(){{
            add(new ArrayList<String>(){{ add("p1"); add("10"); add("5"); }});
            add(new ArrayList<String>(){{ add("p2"); add("5"); add("15"); }});
            add(new ArrayList<String>(){{ add("p3"); add("17"); add("45"); }});
        }};

        getPageData(items, 1, 1, 2, 1)
        .forEach(System.out::println);

//        List<Item> items2 = new ArrayList<Item>(){{
//            add(new Item("p1", 10, 5));
//            add(new Item("p1", 10, 2));
//            add(new Item("p3", 17, 45));
//        }};
//
//        getPageData2(items2, 1, 2, 1)
//                .forEach(System.out::println);
    }

    private static List<String> getPageData(List<List<String>> items, int sortOrder, int sortParameter,
                                     int itemsPerPage, int pageNumber) {
        System.out.println(items);

        Comparator<List<String>> sorter = (o1, o2) -> {
            if (sortParameter > 0) {
                return Integer.compare(Integer.parseInt(o1.get(sortParameter)),
                        Integer.parseInt(o2.get(sortParameter)));
            }
            return o1.get(sortParameter).compareTo(o2.get(sortParameter));
        };

        items.sort(sortOrder == 0 ? sorter : sorter.reversed());

        System.out.println(items);

        int startIndex = itemsPerPage * pageNumber;
        int endIndex = startIndex + itemsPerPage;

        return items.subList(startIndex, endIndex <= items.size() ? endIndex : items.size())
                .stream()
                .map(list -> list.get(0))
                .collect(Collectors.toList());
    }

    private static List<String> getPageData2(List<Item> items, int sortOrder, int itemsPerPage, int pageNumber) {
        System.out.println(items);

        Comparator<Item> sorter = Comparator.comparing(Item::getId)
                                    .thenComparing(Item::getName)
                                    .thenComparing(Item::getAge);
        items.sort(sortOrder == 0 ? sorter : sorter.reversed());

        System.out.println(items);

        int startIndex = itemsPerPage * pageNumber;
        int endIndex = startIndex + itemsPerPage;

        return items.subList(startIndex, endIndex <= items.size() ? endIndex : items.size())
                .stream()
                .map(Item::getName)
                .collect(Collectors.toList());
    }

}

class Item {
    private String name;
    private int id;
    private int age;

    @Override
    public String toString() {
        return "Item{" +
                "name='" + name + '\'' +
                ", id=" + id +
                ", age=" + age +
                '}';
    }

    public Item(String name, int id, int age) {
        this.name = name;
        this.id = id;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
}