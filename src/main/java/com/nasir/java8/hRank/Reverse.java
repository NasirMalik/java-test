package com.nasir.java8.hRank;

import java.util.*;
import java.util.stream.Collectors;

public class Reverse {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        List<String> intList = Arrays.stream(arr)
                .boxed()
                .map(Objects::toString)
                .collect(Collectors.toList());
        Collections.reverse(intList);

        System.out.println(
                   intList.stream().collect(Collectors.joining(" "))
            );

        scanner.close();
    }

}
