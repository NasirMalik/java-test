package com.nasir.java8.hRank;

/**
 * Jumping on the Clouds
 */

import java.io.*;
import java.util.*;

public class CloudJumper {

    static List<Integer> compareTriplets(List<Integer> a, List<Integer> b) {
        List<Integer> finalList = new ArrayList<Integer>(){{
            add(0);
            add(0);
        }};
        for (int i=0; i<3; i++) {
            if (a.get(i) > b.get(i)) finalList.set(0, finalList.get(0)+1);
            if (a.get(i) < b.get(i)) finalList.set(1, finalList.get(1)+1);
        }
        return finalList;
    }

    static long arraySum(long[] ar) {
        return Arrays.stream(ar).sum();
    }

    // Complete the jumpingOnClouds function below.
    static int jumpingOnClouds(int[] c) {

        List<Integer> steps = new ArrayList<>();
        boolean thundercloud = false;
        for (int i = 0; i < c.length; i++) {
            if (steps.isEmpty() && c[i] == 0)  {
                steps.add(i);
                continue;
            }
            if (c[i] == 1) {
                thundercloud = true;
                continue;
            }

            if (!thundercloud && i+1 < c.length && c[i+1] == 0) {
                steps.add(++i);
                thundercloud = false;
                continue;
            }
            if (c[i] == 0) {
                steps.add(i);
                thundercloud = false;
            }
        }
        System.out.println(steps);
        return steps.size()-1;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String[] cItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] c = new int[n];

        for (int i = 0; i < n; i++) {
            int cItem = Integer.parseInt(cItems[i]);
            c[i] = cItem;
        }

        int result = jumpingOnClouds(c);

        System.out.println(result);

        scanner.close();
    }
}

