package com.nasir.java8.hRank;

import java.util.*;

public class ScoreGrade {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        String firstName = scan.next();
        String lastName = scan.next();
        int id = scan.nextInt();
        int numScores = scan.nextInt();
        int[] testScores = new int[numScores];
        for(int i = 0; i < numScores; i++){
            testScores[i] = scan.nextInt();
        }
        scan.close();

        Student s = new Student(firstName, lastName, id, testScores);
        s.printPerson2();
        System.out.println("Grade: " + s.calculate() );
    }
}

class Person2 {
    protected String firstName;
    protected String lastName;
    protected int idNumber;

    // Constructor
    Person2(String firstName, String lastName, int identification){
        this.firstName = firstName;
        this.lastName = lastName;
        this.idNumber = identification;
    }

    // Print Person2 data
    public void printPerson2(){
        System.out.println(
                "Name: " + lastName + ", " + firstName
                        + 	"\nID: " + idNumber);
    }

}

class Student extends Person2{
    private int[] testScores;

    Student(String firstName, String lastName, int identification, int[] scores) {
        super(firstName, lastName, identification);
        this.testScores = scores;
    }

    public String calculate() {
        int average = Arrays.stream(testScores).sum() / testScores.length;

        if (average >= 90 && average <= 100){
            return "O";
        }
        else if (average >= 80){
            return "E";
        }
        else if (average >= 70){
            return "A";
        }
        else if (average >= 55){
            return "P";
        }
        else if (average >= 40){
            return "D";
        }
        return "T";
    }

    /*
     *   Class Constructor
     *
     *   @param firstName - A string denoting the Person2's first name.
     *   @param lastName - A string denoting the Person2's last name.
     *   @param id - An integer denoting the Person2's ID number.
     *   @param scores - An array of integers denoting the Person2's test scores.
     */
    // Write your constructor here

    /*
     *   Method Name: calculate
     *   @return A character denoting the grade.
     */
    // Write your method here
}


