package com.nasir.java8.hRank;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WithinTags {

    static Pattern p = Pattern.compile("^<(.+)>(.+)<\\/\\1>");

    public static void main(String[] args){

        Scanner in = new Scanner(System.in);
        int testCases = Integer.parseInt(in.nextLine());
        while(testCases>0){
            String line = in.nextLine();

            Matcher m = p.matcher(line);
            boolean matched = false;
            while (m.find()) {
                if (!matchInner(m.group(2))){
                    System.out.println(m.group(2));
                }
                matched = true;
            }
            if (!matched) {
                System.out.println("None");
            }
            testCases--;
        }
    }

    public static boolean matchInner(String matchedStr) {
        Matcher m = p.matcher(matchedStr);
        boolean matched = false;
        while (m.find()) {
            matched = true;
            if (!matchInner(m.group(2))){
                System.out.println(m.group(2));
            }
        }
        return matched;
    }

}
