package com.nasir.java8.hRank;

import java.io.*;
import java.math.*;
import java.security.*;
import java.text.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.regex.*;


public class RepeatedString {
    // Complete the repeatedString function below.
    static long repeatedString(String s, long n) {
        long occurrences = 0;
        for (int i = 0; i < s.length(); i++) {
            if ('a' == s.charAt(i)) {
                occurrences++;
            }
        }
        occurrences *= n/s.length();
        s = s.substring(0, (int) (n % s.length()));
        for (int i = 0; i < s.length(); i++) {
            if ('a' == s.charAt(i)) {
                occurrences++;
            }
        }
        return occurrences;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        String s = scanner.nextLine();

        long n = scanner.nextLong();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        System.out.println(repeatedString(s, n));

        scanner.close();
    }
}

