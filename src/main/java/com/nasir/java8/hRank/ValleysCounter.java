package com.nasir.java8.hRank;

import java.util.Scanner;

public class ValleysCounter {

    // Complete the countingValleys function below.
    static int countingValleys(int n, String s) {
        int valleyCount = 0;
        boolean valleyStart = false;
        int level = 0;

        for (char c: s.toCharArray()){
            level = (c == 'U') ? level+1 : level-1;

            if (valleyStart) {
                if (level == 0) {
                    valleyStart = false;
                    valleyCount++;
                }
            }
            else {
                if (level == -1) {
                    valleyStart = true;
                }
            }
        }

        return valleyCount;
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String s = scanner.nextLine();

        System.out.println(countingValleys(n, s));

        scanner.close();
    }

}
