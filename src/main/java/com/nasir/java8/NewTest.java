package com.nasir.java8;

import com.nasir.java8.hRank.NewYearChaos;

import java.io.*;
import java.time.LocalDate;
import java.time.Year;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;

//class Parent {
//    protected static int count = 0;
//    public Parent() {
//        count++;
//    }
//    static int getCount() {
//        return count;
//    }
//}
//public class NewTest extends Parent {

public class NewTest {

//    void display() {
//        System.out.println("data = " + data);
//    }
//
//    int data;

//    public NewTest() {
//        count++;
//    }

    public static void main(String[] args) {

//        System.out.println("Count = " + getCount());
//        NewTest newTest = new NewTest();
//        System.out.println("Count = " + getCount());

//        Arrays.stream(args).forEach(System.out::println);

//        StringBuilder bufferc = new StringBuilder("C");
//        Formatter fmt = new Formatter(bufferc);
//
//
//        fmt.format("%s%s", "A", "B");
//        System.out.println(fmt);
//
//        fmt.format("%-2s", "B");
//        System.out.println(fmt);
//
//        fmt.format("%b", null);
//        System.out.println(fmt);

//        int x = 0;
//        new NewTest().display();

//        int a = 9, b =2;
//        float f = a/b;
//        System.out.println(f);
//        f = f/2;
//        System.out.println(f/2);
//        f = a + b/f;
//        System.out.println((a + b/f));

//        LocalDate localDate = LocalDate.of(2015, 4, 4);
//        System.out.println(localDate.format(DateTimeFormatter.ofPattern("MMM dd, yyyy")));
//        System.out.println(localDate.format(DateTimeFormatter.ofPattern("E, MMM dd, yyyy")));
//        System.out.println(localDate.format(DateTimeFormatter.ofPattern("MM/dd/yy")));

//        Supplier<String> i = () -> "Car";
//        Consumer<String> c = x -> System.out.println(x.toLowerCase());
//        Consumer<String> d = x -> System.out.println(x.toUpperCase());
//        c.andThen(d).accept(i.get());
//        System.out.println();

//        byte c1[] = {10, 20, 30, 40, 50};
//        byte c2[] = {60, 70, 80, 90};
//        ByteArrayOutputStream b2 = new ByteArrayOutputStream(10);
//        b2.write(100);
//        System.out.println(b2.size());
//        b2.write(c1, 0, c2.length);
//        System.out.println(b2.size());

//        List<?> testList = new ArrayList<String>();

//        int year = 2015;
//        System.out.println(Year.isLeap(year));
//
//        Calendar cal = Calendar.getInstance();
//        cal.set(Calendar.YEAR, year);
//        System.out.println(cal.getActualMaximum(Calendar.DAY_OF_YEAR));
//
//        Calendar cal = Calendar.getInstance();
//        cal.set(year, 1, 1);
//        System.out.println(cal.getActualMaximum(Calendar.DAY_OF_MONTH));

//        int x = 5;
//        System.out.println(x = ~1);

//        File file = new File("output.txt");
//        file.delete()
//        try {
//            FileWriter writer = new FileWriter(file);
//            for (int i = 0; i < 5; i++) {
//                file.setWritable(i);
//                file.toString();
//                writer.write(String.valueOf(i));
//            }
//            Stream.of("0", "1").forEach(writer::write);
//            PrintWriter printer = new PrintWriter(writer);
//            Stream.of("0", "1").forEach(printer::write);
//
//            writer.write(new char[]{'0', '1'});
//        } catch (IOException e) {
//            e.printStackTrace();
//        }

//        try {
//            String message = getMessage();
//            System.out.println(message);
//        }
//        catch (Exception e) {
//            message += "F";
//            System.out.print(message);
//        }
//        finally {
//            System.out.println(message);
//        }

//        Double d = new Double("17.46d");
//        System.out.println(d);
//
//        Float f = new Float(23.43);
//        System.out.println(f);
//
//        Character c = new Character("C");
//
//        Integer i = Integer.parseUnsignedInt(4);
//
//        Boolean b = new Boolean("false");

//        getInstance();

//        System.out.println("" + doSum());
//        System.out.println("" + doMinus());
//
//        Class.forName()

//        final String s1 = "HI";
//        final Integer i1 = 3;
//        final int x = 1;
//
//        MyPred<Object>[] p = new MyPred[4];
//        p[0] = new MyPred<Object>(e -> e.equals(s1));

//        Locale l = new Locale(Locale.ENGLISH);
//        Locale l = new Locale("", "");
//        Locale l = new Locale();
//        Locale l = new Locale("ENGLISH");
//        Locale l = new Locale(Locale.ENGLISH, Locale.US);

//        Integer before = new Integer(25);
//        Integer after = ++before == 26?5: new Integer(0);
//        System.out.println(after.intValue() - before.intValue());

//        Integer x = 3, y = null;
//        try {
//            System.out.println(Integer.compareUnsigned(x, 3) == 0 || Integer.compareUnsigned(y, 0) == 0);
//        }
//        catch (Exception e) {
//            System.out.println(e.getClass().toString());
//        }
//
//        try {
//            System.out.println(y.compareTo(null) == 0 || true);
//        }
//        catch (Exception e) {
//            System.out.println(e.getClass().toString());
//        }

//        final NewTest newTest = new NewTest();
//        for (int i = 0; i < 1000; i++) {
//            new Thread(() -> {
//                newTest.increment();
//            }, "Thread" + i);
//            try {
//                Thread.sleep(5000);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            System.out.println("Final count : " + newTest.getCount());
//        }

        Set<String> set = new LinkedHashSet<String>();
        set.add("3");
        set.add("1");
        set.add("3");
        set.add("2");
        set.add("3");
        set.add("1");
        set.forEach(s -> System.out.print(s + "-"));
    }
    /*
    main
     */

//private AtomicLong count;
//    public void increment() {
//        count.getAndIncrement();
//    }
//    public long getCount() {
//        return count.get();
//    }

//interface DefaultInt {
//    default void func1() {
//        System.out.println("Default in Interface");
//    }
//}
//
//interface Test3 extends DefaultInt {
//        public  void func1();
//
//}

//    static boolean testIt(Object s, Predicate<Object> p) {
//        return p.test(s);
//    }
//
//    class MyPred<T> implements Predicate<T> {
//        Predicate<T> local;
//
//        public MyPred(Predicate<T> t) {
//            local = t;
//        }
//
//        @Override
//        public boolean test(T t) {
//            return local.test(t);
//        }
//    }

//    static int number2 = getValue();
//    static int number1 = 10;
//    static int getValue() {
//        return number1;
//    }
//
//    static int doSum() {
//        return number1 + number2;
//    }
//
//    static int doMinus() {
//        return number1 - number2;
//    }

//    private static NewTest newTest = new NewTest();
//    public synchronized static NewTest getInstance() {
//        throw new IllegalArgumentException();
////        return newTest;
//    }



//    private static String getMessage() throws Exception {
//        String message = "A";
//        try {
//            throw new Exception();
//        }
//        catch (Exception e) {
//            message += "B";
//        }
//        finally {
//            message += "C";
//        }
//        return message;
//    }
//
//    static String message;

}

//class SendDocument {
//    {
//        System.out.println("In SendDocument");
//    }
//}

//interface A extends AutoCloseable, Serializable {
//
//}
//
//abstract class Test2 {
//    protected abstract Number getLocale(String value, boolean isValid);
//}
//
//class Test2Impl extends Test2 {
//
//    @Override
//    public Integer getLocale(String value, boolean isValid) {
//        return null;
//    }
//}
