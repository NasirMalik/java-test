package com.nasir.java8.lambda;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.function.Predicate;

public class LambdaTest {

    public static void main(String[] args) {
        testFileFilterInLambda();
    }

    /**
     * Shows how we can use the FileFilter as lambda instead of an anonymous class
     */
    public static void testFileFilterInLambda(){
        File reportFolder = new File("/Users/nasir/Downloads");

        // # -1
        FileFilter filter = new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        };
        Arrays.asList(reportFolder.listFiles(filter)).forEach(System.out::println);

        // # 0
        Arrays.asList(reportFolder.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        })).forEach(System.out::println);

        // # 1
        Predicate<File> fileTest = File::isFile;

//        Arrays.asList(reportFolder.listFiles(file -> fileTest.test(file)))
        Arrays.asList(reportFolder.listFiles(fileTest::test))
                .stream()
                .map(File::getName)
                .forEach(System.out::println);

        // # 2
        Arrays.asList(reportFolder.listFiles(file -> file.isDirectory()))
                .forEach(System.out::println);

        // # 3
        Arrays.asList(reportFolder.listFiles(File::isDirectory))
                .forEach(System.out::println);

        // # 4
        List<String> starList = Arrays.asList("****", "*******", "*", "******");
        Collections.sort(starList, (s1, s2) -> Integer.compare(s2.length(), s1.length()));
        starList.forEach(System.out::println);

        // # 5
        List<Integer> intList = Arrays.asList(4, 4,1, 9, 23, 12, 1);
        Collections.sort(intList, Integer::compare);
        intList.forEach(System.out::println);
    }

}
