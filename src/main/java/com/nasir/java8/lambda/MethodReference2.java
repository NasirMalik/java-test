package com.nasir.java8.lambda;

public class MethodReference2 {
    public static void ThreadStatus(){
        System.out.println("Thread is running...");
    }
    public static void main(String[] args) {
        Thread t2=new Thread(MethodReference2::ThreadStatus);

        Thread t3=new Thread();
        t2.start();
    }
}
