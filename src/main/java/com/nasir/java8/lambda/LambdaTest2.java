package com.nasir.java8.lambda;

import java.util.function.Consumer;

interface Walkable {
    void walk();
}

class Human implements Walkable {

    @Override
    public void walk() {
        System.out.println("Human walking");
    }

}

class Robot implements Walkable {

    @Override
    public void walk() {
        System.out.println("Robot walking");
    }

}

public class LambdaTest2 {

    public static void main(String[] args) {
//        testNormalPolyMorph();
//        testAnonymousClass();
//        testLambda();
        testLambdaWithConsumer();
    }

    private static void testLambdaWithConsumer() {
        walker.accept(() -> System.out.println("Human walking"));
        walker.accept(() -> System.out.println("Robot walking"));
    }

    private static void testLambda() {
        walker(() -> System.out.println("Human walking"));
        walker(() -> System.out.println("Robot walking"));
    }

    private static void testAnonymousClass() {
        Walkable jack = new Walkable() {
            @Override
            public void walk() {
                System.out.println("Human walking");
            }
        };
        walker(jack);

        Walkable wale = new Walkable() {
            @Override
            public void walk() {
                System.out.println("Robot walking");
            }
        };
        walker(wale);
    }

    private static void testNormalPolyMorph() {
        Walkable jack = new Human();
        walker(jack);

        Walkable wale = new Robot();
        walker(wale);
    }

    private static void walker(Walkable walker) {
        walker.walk();
    }

    private static Consumer<Walkable> walker = Walkable::walk;

}
