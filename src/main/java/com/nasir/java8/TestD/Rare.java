package com.nasir.java8.TestD;

import java.util.*;
import java.util.stream.Collectors;

public class Rare {

    public static int nthMostRare(int[] elements, int n) {

        Map<Integer, Integer> sortedMap = new HashMap<>();

        for (int element : elements) {
            int count = 1;
            if (!sortedMap.isEmpty()){
                if (sortedMap.containsKey(element)) {
                    count = sortedMap.get(element)+1;
                }
            }
            sortedMap.put(element, count);
        }

        sortedMap = sortedMap.entrySet()
                .stream()
                .sorted((Map.Entry.<Integer, Integer>comparingByValue()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));

//        int nth = 0;
//        for (Integer key : sortedMap.keySet()) {
//            nth++;
//            if (nth == n) {
//                return key;
//            }
//        }

        return (int) sortedMap.keySet().toArray()[n-1];

//        return 0;
    }

//    public static int nthMostRare(int[] elements, int n) {
//
//        Map<Integer, Long> counted = Arrays.stream(elements).boxed()
//                                        .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
//
//        System.out.println(counted);
//
//        List<Integer> countedSortedKey = counted.entrySet().stream().sorted(Map.Entry.<Integer, Long>comparingByValue())
//                .map(Map.Entry::getKey)
//                .collect(Collectors.toList());
//
//        System.out.println(countedSortedKey);
//
//        return countedSortedKey.get(n-1);
//    }

//    public static int nthMostRare(int[] elements, int n) {
//
//        return Arrays.stream(elements)
//                .boxed()
//                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
//                .entrySet()
//                    .stream()
//                    .sorted(Map.Entry.comparingByValue())
//                    .map(Map.Entry::getKey)
//                    .collect(Collectors.toList())
//                        .get(n-1);
//    }

    public static void main(String[] args) {
        int x = nthMostRare(new int[] { 5, 4, 2, 3, 1, 5, 4, 3, 2, 6, 4, 3, 5, 4, 5 }, 4);
        System.out.println(x);
    }
}
