/**
 *

 Implement a function folderNames, which accepts a string containing an XML file that specifies folder structure and returns all folder names that start with startingLetter. The XML format is given in the example below.

 For example, for the letter 'u' and an XML file:

 <?xml version="1.0" encoding="UTF-8"?>
 <folder name="c">
 <folder name="program files">
 <folder name="uninstall information" />
 </folder>
 <folder name="users" />
 </folder>

 the function should return a collection with items "uninstall information" and "users" (in any order).

 */
package com.nasir.java8.TestD;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;

public class Folders {

    public static List<String> folderNames(String xml, String tagName, String attributeName) throws Exception {
        xml = xml.replaceAll("\\\"", "");
        DocumentBuilderFactory db = DocumentBuilderFactory.newInstance();
        DocumentBuilder build = db.newDocumentBuilder();
        Document document = build.parse(new InputSource(new StringReader(xml)));
        document.getDocumentElement().normalize();

        NodeList nodes = document.getElementsByTagName(tagName);
        List<String> folders = new ArrayList<>();
        for (int i=0; i<nodes.getLength(); i++) {
            Optional<Node> folderName = Optional.ofNullable(nodes.item(i).getAttributes().getNamedItem(attributeName));
            folderName.ifPresent(
                    node -> folders.add(node.getNodeValue()));
        }
        return folders;
    }

    public static void main(String[] args) throws Exception {
        String xml =
                "<?xml version='1.0' encoding='UTF-8'?>" +
                        "<folder>" +
                        "<folder name='program files'>" +
                        "<folder name='uninstall information' />" +
                        "</folder>" +
                        "<folder name='users' />" +
                        "</folder>";

        Collection<String> names = folderNames(xml, "folder", "name");
        for(String name: names)
            System.out.println(name);
    }

}