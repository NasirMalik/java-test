package com.nasir.java8.TestD;

import java.util.*;
import java.text.SimpleDateFormat;

public class MovieNight {

//    public static Boolean canViewAll(Collection<Movie> movies) {
//        for (Movie movie: movies) {
//            for (Movie movie1: movies) {
//                if (movie1 != movie) {
//                    if (!(movie.getStart().getTime() >= movie1.getEnd().getTime())
//                            && !(movie.getEnd().getTime() <= movie1.getStart().getTime())) {
//                        return false;
//                    }
//                }
//
//            }
//        }
//        return true;
//    }

    public static Boolean canViewAll(Collection<Movie> movies) {

        List<Movie> moviesList = (ArrayList<Movie>) movies;
        moviesList.sort(Comparator.comparing(Movie::getStart));

        for (int i=0; i<movies.size()-1; i++) {
            if (moviesList.get(i).getEnd().getTime() > moviesList.get(i+1).getStart().getTime()) {
                return false;
            }
        }

        return true;
    }

    public static void main(String[] args) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("y-M-d H:m");

        ArrayList<Movie> movies = new ArrayList<Movie>();

        movies.add(new Movie(sdf.parse("2015-01-01 20:00"), sdf.parse("2015-01-01 21:30")));
        movies.add(new Movie(sdf.parse("2015-01-01 23:10"), sdf.parse("2015-01-01 23:30")));
        movies.add(new Movie(sdf.parse("2015-01-01 21:30"), sdf.parse("2015-01-01 23:10")));

        System.out.println(MovieNight.canViewAll(movies));
    }
}

class Movie {
    private Date start, end;

    public Movie(Date start, Date end) {
        this.start = start;
        this.end = end;
    }

    public Date getStart() {
        return this.start;
    }

    public Date getEnd() {
        return this.end;
    }

}
