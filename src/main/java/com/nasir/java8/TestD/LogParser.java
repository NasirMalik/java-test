package com.nasir.java8.TestD;

import java.io.BufferedReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collection;

public class LogParser {
    public static Collection<Integer> getIdsByMessage(String xml, String message) throws Exception {
        Collection<Integer> ids = new ArrayList<>();

        int lastId = 0;

        BufferedReader reader = new BufferedReader(new StringReader(xml));

        String line = reader.readLine();

        while ((line = reader.readLine()) != null) {
            if (line.trim().startsWith("<entry")) {
                lastId = Integer.parseInt(line.trim()
                        .replace("<entry id=\"", "")
                        .replace("\">", ""));
            }
            if (line.trim().equalsIgnoreCase("<message>" + message + "</message>")) {
                ids.add(lastId);
            };
        }
        return ids;
    }

    public static void main(String[] args) throws Exception {
        String xml =
                "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                        "<log>\n" +
                        "    <entry id=\"1\">\n" +
                        "        <message>Application started</message>\n" +
                        "    </entry>\n" +
                        "    <entry id=\"2\">\n" +
                        "        <message>Application ended</message>\n" +
                        "    </entry>\n" +
                        "</log>";

        Collection<Integer> ids = getIdsByMessage(xml, "Application ended");
        for(int id: ids)
            System.out.println(id);
    }
}