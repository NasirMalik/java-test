/**
 * A playlist is considered a repeating playlist if any of the songs contain a reference to a previous song in the playlist. Otherwise, the playlist will end with the last song which points to null.
 *
 * Implement a function isRepeatingPlaylist that, efficiently with respect to time used, returns true if a playlist is repeating or false if it is not.
 *
 * For example, the following code prints "true" as both songs point to each other.
 */

package com.nasir.java8.TestD;

public class Song {
    private String name;
    private Song nextSong;

    public Song(String name) {
        this.name = name;
    }

    public void setNextSong(Song nextSong) {
        this.nextSong = nextSong;
    }

    public boolean isRepeatingPlaylist() {
        if (this.nextSong == null) {
            return false;
        }
        else {
            Song tempNextSong = this.nextSong;
            while (tempNextSong != null && !this.equals(tempNextSong)) {
                tempNextSong = tempNextSong.nextSong;
            }
            return tempNextSong != null;
        }
    }

    public static void main(String[] args) {
        Song first = new Song("Hello");
        Song second = new Song("Eye of the tiger");
        Song third = new Song("Third Song");

        first.setNextSong(second);
        second.setNextSong(third);
        third.setNextSong(null);

        System.out.println(first.isRepeatingPlaylist());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Song song = (Song) o;

        if (!name.equals(song.name)) return false;
        return nextSong.equals(song.nextSong);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + nextSong.hashCode();
        return result;
    }
}
