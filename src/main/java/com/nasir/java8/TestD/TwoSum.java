/**
 *

 Write a function that, when passed a list and a target sum, returns, efficiently with respect to time used, two distinct zero-based indices of any two of the numbers, whose sum is equal to the target sum. If there are no two numbers, the function should return null.

 For example, findTwoSum(new int[] { 3, 1, 5, 7, 5, 9 }, 10) should return a single dimensional array with two elements and contain any of the following pairs of indices:

 0 and 3 (or 3 and 0) as 3 + 7 = 10
 1 and 5 (or 5 and 1) as 1 + 9 = 10
 2 and 4 (or 4 and 2) as 5 + 5 = 10


 */

package com.nasir.java8.TestD;

public class TwoSum {
    public static int[] findTwoSum(int[] list, int sum) {
        int[] response = new int[2];
        boolean found = false;
        int startingIndex = 0;

        do {
            for (int i = startingIndex; i < list.length; i++) {
                if (!found && list[i] < sum) {
                    response[0] = i;
                    found = true;
                }
                else if (found && (list[response[0]] + list[i]) == sum) {
                    response[1] = i;
                    return response;
                }
            }
            if (found) {
                startingIndex = response[0]+1;
                found = false;
            }
            else break;
        } while (!found);

        return null;
    }

    public static void main(String[] args) {
        int[] indices = findTwoSum(new int[] { 2, 1, 5, 7, 5, 9 }, 10);
        if(indices != null) {
            System.out.println(indices[0] + " " + indices[1]);
        }
        else {
            System.out.println("Not Found");
        }
    }
}

/**
 * Example case: Correct answer
 *   Distinct numbers with and without solutions: Correct answer
 *   Duplicate numbers with and without solutions: Correct answer
 *   Performance test with a large list of numbers: Time limit exceeded
 */
