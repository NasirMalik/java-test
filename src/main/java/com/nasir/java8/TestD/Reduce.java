package com.nasir.java8.TestD;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Reduce {

    public static void main(String[] args) {
        long then = System.currentTimeMillis();
//        System.out.println(then);
//        System.out.println("\nReduction is : " + reduce2("eqrwqerwerhjtVvwejhfvnmsvm hjertuweguhkqkit7t787t8887786787324gqwjgjhq   ej  efasq3434433455555"));
//        long now = System.currentTimeMillis();
//        System.out.println(now);
//        then = now;

//        String input = "Bye bye";
//        String input = "Hello hello Ab aB";
        String input = "eqrwqerwerhjtVvwejhfvnmsvm hjertuweguhkqkit7t787t8887786787324gqwjgjhq   ej  efasq3434433455555";

        System.out.println(then);
        System.out.println("Reduction is : " + reduce1(input));
        long now = System.currentTimeMillis();
        System.out.println(now);
    }



    public static String reduce3(String s) {
        Pattern p = Pattern.compile("\\b(\\w+)\\b(\\s+\\b\\1\\b)+", Pattern.CASE_INSENSITIVE);

        Matcher m = p.matcher(s);
        boolean matched = false;
        while (m.find()) {
            if (!matched) matched = true;
            s = s.replaceAll(m.group(), m.group(1));
            System.out.println(m.toString() + "\t: " + m.group() + "\t: " + s);
        }
        return s.trim();
    }

    public static String reduce1(String s) {
        Pattern p = Pattern.compile("(.)\\1{1}", Pattern.CASE_INSENSITIVE);

        Matcher m = p.matcher(s);
        boolean matched = false;
        while (m.find()) {
            s = s.replaceAll(m.group(), "");
            System.out.println(m.group() + "\t: " + s);
        }
        return s.trim();
    }

    public static String reduce2(String s) {
        if (s.length() == 0) return s;
        int startingIndex = 0;
        while (true) {
            boolean found = false;
            for (int i=startingIndex; i<s.length()-1; i++) {
                if (s.charAt(i) == s.charAt(i+1)) {
                    found = true;
                    s = s.replaceFirst(("" + s.charAt(i) + s.charAt(i)), "");
                    if (s.length() <= 1) return s;
                    System.out.println(s);
                    startingIndex = i;
                    break;
                }
            }
            if (!found) return s;
        }
    }

}
