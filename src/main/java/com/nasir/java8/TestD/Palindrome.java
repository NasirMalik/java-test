/**
 *

 A palindrome is a word that reads the same backward or forward.

 Write a function that checks if a given word is a palindrome. Character case should be ignored.

 For example, isPalindrome("Deleveled") should return true as character case should be ignored, resulting in "deleveled", which is a palindrome since it reads the same backward and forward.

 */

package com.nasir.java8.TestD;

public class Palindrome {
    public static boolean isPalindrome(String word) {
        StringBuilder reverse = new StringBuilder(word).reverse();
        return reverse.toString().equalsIgnoreCase(word);
    }

    public static void main(String[] args) {
        System.out.println(Palindrome.isPalindrome("Deleveled"));
    }
}
