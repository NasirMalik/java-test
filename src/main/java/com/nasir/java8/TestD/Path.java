package com.nasir.java8.TestD;

/**
 *

 Write a function that provides change directory (cd) function for an abstract file system.

 Notes:

 Root path is '/'.
 Path separator is '/'.
 Parent directory is addressable as "..".
 Directory names consist only of English alphabet letters (A-Z and a-z).
 The function should support both relative and absolute paths.
 The function will not be passed any invalid paths.
 Do not use built-in path-related functions.

 For example:

 Path path = new Path("/a/b/c/d");
 path.cd('../x');
 System.out.println(path.getPath());

 should display '/a/b/c/x'.

 */

public class Path {
    private String path;

    public Path(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    public void cd(String newPath) {
        if (!path.replaceAll("/", "").matches("[a-zA-Z]+")){
            System.out.println("Invalid path, contains numbers");
            return;
        }

        if (newPath.startsWith("/")) {
            path = newPath;
        }
        else {
            while (newPath.startsWith("..")) {
                newPath = newPath.replaceFirst("../", "");
                int lastSlash = path.lastIndexOf("/");
                if (lastSlash > -1) {
                    path = path.substring(0, lastSlash);
                }
            }
            path = path + "/" + newPath;
        }

    }

    public static void main(String[] args) {
        Path path = new Path("/Users/nasir/dev");
        path.cd("/logs/wuas");
        System.out.println(path.getPath());
    }


}

/**
 Example case: Correct answer
 Selecting child directories: Correct answer
 Selecting parent directories: Time limit exceeded
 Selecting complex paths: Correct answer
 */