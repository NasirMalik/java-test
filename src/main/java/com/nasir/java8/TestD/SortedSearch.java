/**
 *

 Implement function countNumbers that accepts a sorted array of unique integers and, efficiently with respect to time used, counts the number of array elements that are less than the parameter lessThan.

 For example, SortedSearch.countNumbers(new int[] { 1, 3, 5, 7 }, 4) should return 2 because there are two array elements less than 4.

 */

package com.nasir.java8.TestD;

public class SortedSearch {


//    public static int countNumbers(int[] sortedArray, int lessThan) {
//        if (sortedArray == null || sortedArray.length == 0) {
//            return 0;
//        }
//        if (sortedArray.length == 1) {
//            return (sortedArray[0] >= lessThan) ? 0 : 1;
//        }
//
//        List<Integer> sortedList = (List) Arrays.stream(sortedArray).collect(Collectors.toList());
//        for (int i = lessThan-1; )

//        int start = 0;
//        int end = sortedArray.length;
//        boolean found = false;
//        int less = 0;
//
//        int mid = (end - start) / 2;
//
//        if (sortedArray[mid] > lessThan) {
//            less += 0;
//            for (int i = 0; i <= mid; i++) {
//                if (sortedArray[i] < lessThan) less++;
//                else break;
//            }
//        } else {
//            less += mid + 1;
//            for (int i = mid + 1; i < sortedArray.length; i++) {
//                if (sortedArray[i] < lessThan) less++;
//                else break;
//            }
//        }

//        do {
//            int mid = (end - start) / 2;
//
//            if (sortedArray[start+mid] > lessThan) {
//                start = start;
//                end = start+mid;
//                less += 0;
//            } else {
//                less += mid + 1;
//                start = start+mid+1;
//                end = sortedArray.length;
//            }
//            if(end - start < 1) {
//                found = true;
//            }
//        }
//        while (!found);

//        return less;

//        if (sortedArray[mid-1] > lessThan) {
//            return mid == 1
//                    ? countNumbers(Arrays.copyOfRange(sortedArray, 0, mid), lessThan)
//                    : countNumbers(Arrays.copyOfRange(sortedArray, 0, mid-1), lessThan) ;
//        }
//        else if (sortedArray[mid-1] < lessThan) {
//            return mid + countNumbers(Arrays.copyOfRange(sortedArray, mid, sortedArray.length), lessThan);
//        }
//        return 0;
    }

//    public static void main(String[] args) {
//        System.out.println(SortedSearch.countNumbers(new int[] { 1, 2, 3, 5, 6, 7, 8 }, 7));
//    }
//}

/**
 Example case: Correct answer
 Various small arrays: Wrong answer
 Performance test when sortedArray contains lessThan: Wrong answer
 Performance test when sortedArray doesn't contain lessThan: Wrong answer
 */