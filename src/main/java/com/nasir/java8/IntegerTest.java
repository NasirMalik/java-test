package com.nasir.java8;

public class IntegerTest {

    int refNum;

    public int getRefNum() {
        return refNum;
    }

    public void setRefNum(int refNum) {
        this.refNum = refNum;
    }

    public static void main(String[] args) {
//        IntegerTest test = new IntegerTest();
//        test.setRefNum(Integer.parseInt("901088481"));
//        System.out.println(test.getRefNum());
//        System.out.println(test.getRefNum() + 1);

        testArrayInitAndPrint();
    }

    private static void testArrayInitAndPrint() {
        int[] a = new int[5];
        for (int i : a) {
            System.out.print(i);
        }
    }

}
