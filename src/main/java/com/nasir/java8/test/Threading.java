package com.nasir.java8.test;

import java.util.stream.IntStream;

public class Threading {

    public static void main(String[] args) throws InterruptedException {
        testThreads();
//        Threading tth = new Threading();
//        Class c = tth.getClass();
//        try {
//            System.out.println(c.getMethod("getNumber", null).toString());
//            System.out.println(c.getDeclaredMethod("setNumber", null).toString());
//        } catch (NoSuchMethodException | SecurityException e) {
//        }

//        Supplier<String> i = () -> "Car";
//        Consumer<String> c = x -> System.out.print(x.toLowerCase());
//        Consumer<String> d = x -> System.out.print(x.toUpperCase());
//        c.andThen(d).accept(i.get());
//        System.out.println();

    }

//    public Integer getNumber() {
//        return 2;
//    }
//
//    public void setNumber(Integer number) {
//    }



    public static void testThreads() throws InterruptedException {
        ThreadTest TT = new ThreadTest();
        Runnable r = () -> {
            for (long i = 0; i < 2_147_483_647; i++) {
                TT.incrementCounter();
            }
        };

        Thread t = new Thread(r);
        t.start();
//        t.wait();
        TT.displayCounter();

        IntStream.rangeClosed(1, 5).forEach(iter -> {
            System.out.print(iter + " -> ");
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            TT.displayCounter();
        });

        TT.displayCounter();

        t.join();
        TT.displayCounter();
    }

    static class ThreadTest {
        long counter = 0;

        void incrementCounter(){ counter++; }

        void displayCounter() {
            System.out.printf("Counter is %d%n", counter);
        }
    }
}
