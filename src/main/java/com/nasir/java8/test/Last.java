package com.nasir.java8.test;

import java.math.BigInteger;

/**
 * Created by nasir on 05/9/18.
 */
public class Last {

    public static void main(String[] args) {
        if (args.length == 2) {
            System.out.println(getSumOfProduct(Long.parseUnsignedLong(args[0]), Long.parseUnsignedLong(args[1])));
        }
        else {
            System.out.println("Please provide the arguments for the programs to run i.e N and C");
//            System.out.println(getSumOfProduct(5, 2));
//            System.out.println(getSumOfProduct(100, 10));
//            System.out.println(getSumOfProduct(1000000, 200).toString().substring(0, 10));
//            System.out.println(getSumOfProduct(10000000, 200));
        }
    }

    public static BigInteger getSumOfProduct(long n, long c){

        BigInteger currentProduct = BigInteger.valueOf(1);
        BigInteger sumOfProducts = BigInteger.valueOf(1);

        // one loop across the whole range, which determines the complexity O(n)
        for (int i=3; i<=n; i++) {
            currentProduct = getPreceedingProduct(i, c, currentProduct);
            // add to the running sum
            sumOfProducts = sumOfProducts.add(currentProduct);
        }

        return sumOfProducts;
    }

    private static BigInteger getPreceedingProduct(int index, long c, BigInteger currentProduct) {
        // when the index goes beyond c+1, divide the first of the cth range to remove it from the product
        if (index > c+1){
            currentProduct =  currentProduct.divide(BigInteger.valueOf(index-c-1));
        }
        // then multiply with the cth entry of the range
        currentProduct = currentProduct.multiply(BigInteger.valueOf(index-1));
        return currentProduct;
    }

}
