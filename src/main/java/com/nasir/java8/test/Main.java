package com.nasir.java8.test;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) throws UnsupportedEncodingException {
        testProcess();

        testSleep();

        testCronFrequencyParts("00 06,07-09,11,12,13,14,15,16,17 */2 * 2-4,6");

        testCalender();

        System.out.println(testEncode64("/getApplicant/ug/1093567"));
        System.out.println(testDecode64("L2dldEFwcGxpY2FudC91Zy8xMDkzNTY3"));
        testSubString();

        Main main = new Main();

        main.cellCompete(new int[]{1, 1, 1, 0, 1, 1, 1, 1}, 3);

        main.getGCD(new int[]{4, 5, 2, 6, 4, 10});

        System.out.println(main.leftRotateArray(new int[]{1, 2, 3, 4, 5}, 4));
    }

    public int[] leftRotateArray(int[] a, int d) {
        List<Integer> list = Arrays.stream(a).boxed().collect(Collectors.toList());
        Collections.rotate(list, -1*d);
        return list.stream().mapToInt(i->i).toArray();
    }


    public int getGCD(int[] arr) {
        int gcd = 1;

        List<Integer> list = Arrays.stream(arr).boxed().collect(Collectors.toList());
        System.out.println(list);
        int min = Collections.min(list);

        System.out.println(min);

        Collections.reverse(list);
        System.out.println(list);

        Collections.rotate(list, 2);
        System.out.println(list);

        Collections.rotate(list, 2);
        System.out.println(list);

        Collections.sort(list);
        System.out.println(list);

        return gcd;
    }

    public List<Integer> cellCompete(int[] states, int days) {
        List<Integer> finalStates = new ArrayList<>(8);
        List<Integer> tempStates = Arrays.stream(states).boxed().collect(Collectors.toList());

        System.out.println(tempStates);

        for (int j=1; j<=days; j++) {
            finalStates.clear();
            for (int i = 0; i<tempStates.size(); i++) {
                int left = 0, right = 0;
                if (i == 0) {
                    right = tempStates.get(i+1);
                }
                else if (i == tempStates.size()-1) {
                    left = tempStates.get(i-1);
                }
                else {
                    left = tempStates.get(i-1);
                    right = tempStates.get(i+1);
                }
                finalStates.add(left == right ? 0 : 1);
            }
//            tempStates = finalStates.stream().collect(Collectors.toList());
            Collections.copy(tempStates, finalStates);
            System.out.println(tempStates);
        }
        return finalStates;
    }

    public static void testSubString(){
        System.out.println("1306162784".substring(0, 9));
    }

    public static String testDecode64(String encoded){
        byte[] decoded = Base64.getDecoder().decode(encoded);
        return new String(decoded, StandardCharsets.UTF_8);
    }

    public static String testEncode64(String decoded) throws UnsupportedEncodingException {
        return Base64.getEncoder().encodeToString(decoded.getBytes(StandardCharsets.UTF_8));
    }

    public static void testCalender(){
        Calendar now = Calendar.getInstance();
        System.out.println("Month Day : " + now.get(Calendar.DAY_OF_MONTH));
        System.out.println("Month : " + now.get(Calendar.MONTH));
        System.out.println("Week Day : " + now.get(Calendar.DAY_OF_WEEK));

        System.out.println("Hour : " + now.get(Calendar.HOUR));
        System.out.println("Hour of Day : " + now.get(Calendar.HOUR_OF_DAY));
    }

    public static List<Set<String>> testCronFrequencyParts(String frequency){
        List<Set<String>> frequencyList = new ArrayList<Set<String>>();
        String[] spaceParts = frequency.split(" ");

        for (int i=0; i<5; i++){
            frequencyList.add(new TreeSet<String>());

            if (spaceParts[i].contains("*")){
                String[] slashParts = spaceParts[i].split("/");
                for (Integer dynamic=(i==2 || i==3 ? 1 : 0);
                     dynamic<=(i==0 ? 59 : i==1 ? 23 : i==2 ? 31 : i==3 ? 12 : 6);
                     dynamic += (slashParts.length==2 ? Integer.parseInt(slashParts[1]) : 1)){
                    frequencyList.get(i).add((dynamic<10 && i!=4) ? "0"+dynamic.toString() : dynamic.toString());
                }
            }
            else {
                String[] csParts = spaceParts[i].split(",");
                for (String csPart : csParts){
                    if (!csPart.contains("-")){
                        frequencyList.get(i).add(csPart);
                    }
                    else{
                        String[] hyphonParts = csPart.split("-");
                        for (Integer start = Integer.parseInt(hyphonParts[0]); start <= Integer.parseInt(hyphonParts[1]); start++){
                            frequencyList.get(i).add((start<10 && i!=4) ? "0"+start.toString() : start.toString());
                        }
                    }
                }
            }
            System.out.println(frequencyList.get(i));
        }
        return frequencyList;
    }

    public static void testSleep(){
        try {
            System.out.println("Start Sleep");
            TimeUnit.SECONDS.sleep(10);
            System.out.println("End Sleep");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void testProcess(){
        try {
            List<String> command = new ArrayList<String>(){{
                add("/Users/nasir/project-data/Scheduling/test1.sh");
            }};
            ProcessBuilder processBuilder = new ProcessBuilder(command);
            //        processBuilder.directory(new File("/"));
            File log = new File("/Users/nasir/project-data/Scheduling/log.txt");
            processBuilder.redirectErrorStream(true);
            processBuilder.redirectOutput(ProcessBuilder.Redirect.appendTo(log));
            processBuilder.start();
//            Process p = null;
//            p = processBuilder.start();
//            p.waitFor();
        } catch (IOException e) {
            e.printStackTrace();
        }
//        catch (InterruptedException e) {
//            e.printStackTrace();
//        }
    }
}
