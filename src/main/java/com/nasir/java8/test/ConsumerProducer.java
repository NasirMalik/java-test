package com.nasir.java8.test;

public class ConsumerProducer {

    protected static Object lock = new Object();

    public static void main(String[] args) throws InterruptedException {
        Producer p = new Producer();
        Consumer c = new Consumer();

        Runnable producer = () -> {
            for (int i = 0; i < 50; i++) {
                try {
                    p.produce(lock);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Done Producing : " + p);
        };

        Runnable consumer = () -> {
            for (int i = 0; i < 50; i++) {
                try {
                    c.consume(lock);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Done Consuming : " + c);
        };

        Thread pTask = new Thread(producer);
        Thread cTask = new Thread(consumer);

        pTask.start();
        cTask.start();

        pTask.join();
        cTask.join();

//        System.out.println("Counter after : " + counter);
    }



}

class Base {
    static int[] buffer = new int[10];
    static int counter = 0;

    static boolean isFull() {
        return counter == buffer.length;
    }

    static boolean isEmpty() {
        return counter == 0;
    }
}

class Producer extends Base {

    void produce(Object lock) throws InterruptedException {
        synchronized (lock){
            if (isFull()) {
                System.out.println(Thread.currentThread().getName() + " : waiting : " + counter);
                lock.wait();
            }
            System.out.println(Thread.currentThread().getName() + " : doing : " + counter);
            Thread.sleep(200);
            buffer[counter++] = 1;
            lock.notify();
        }
    }
}


class Consumer extends Base {

    void consume(Object lock) throws InterruptedException {
        synchronized (lock){
            if (isEmpty()) {
                lock.wait();
                System.out.println(Thread.currentThread().getName() + " : waiting : " + counter);
            }
            System.out.println(Thread.currentThread().getName() + " : doing : " + counter);
            lock.wait(200);
            buffer[--counter] = 0;
            lock.notify();
        }
    }
}