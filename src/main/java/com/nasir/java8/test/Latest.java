package com.nasir.java8.test;//import java.util.Arrays;
//import java.util.List;
//import java.util.stream.Collectors;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by nasir on 23/8/18.
 */
public class Latest {

    public static void main(String[] args){
        int[] input = {1, 1, 1, 0, 1, 1, 1, 1};
        new Latest().cellCompete(input, 3);
        int[] input2 = {9, 7, 7, 3, 8, 5, 0, 5, 7, 3, 1};
        System.out.println(new Latest().getMaxDiff(input2));
        int[] input3 = {6, 24, 93, 9};
        System.out.println(new Latest().getMaxNum(input3));
        int[] input4 = {3, 0, 0, 2, 0, 4, 0, 2, 0, 6, 0, 5, 0, 2};
        System.out.println(new Latest().getMaxCapacity(input4));
//                List<List<Integer>> allLocations = new ArrayList<List<Integer>>(){{
//            add(new ArrayList<Integer>() {{ add(1); add(-3); }});
//            add(new ArrayList<Integer>() {{ add(1); add(2); }});
//            add(new ArrayList<Integer>() {{ add(3); add(4); }});
//        }};
//        System.out.print(new com.nasir.java8.test.Last().ClosestXdestinations(3, allLocations, 2));
    }

    /**
     *
     * @param numRows
     * @param numColumns
     * @param area
     * @return
     */
//    int minimumDistance(int numRows, int numColumns, List<List<Integer>> area) {
//        int distanceFromIntersection = 0, totalDistance = 0;
//        List<List<Integer>> intersections = new ArrayList<>();
//
//        for (int x=0; x<numColumns; x++){
//            for  (int y=0; y<numRows; y++) {
//                if (area.get(x+1).get(y) == 1){
//                    distanceFromIntersection++;
//                    intersections.add(new ArrayList<Integer>() {{ add(x); add(y); }});
//                }
//                else if (area.get(x).get(y+1) == 1){
//                    distanceFromIntersection++;
//                    intersections.add(new ArrayList<Integer>() {{ add(x); add(y); }});
//                }
//                else {
//                    intersections.remove(intersections.size()-1);
//                }
//                if (area.get(x).get(y) == 9){
//                    totalDistance+= distanceFromIntersection+1;
//                    break;
//                }
//            }
//            if (area.get(x).get(y) = 9){
//                break;
//            }
//        }
//
//        return totalDistance;
//    }

    /**
     *
     * @param arr
     * @return
     */
    public int getMaxCapacity(int[] arr) {
        int capacity = 0;
        int cursor = 0;

        while (cursor < arr.length-1) {
            int start = arr[cursor];
            int top = 0;
            int index = cursor;

            while (index < arr.length-1 && arr[++index] < start) {
                if (arr[index] > top) {
                    top = arr[index];
                }
            }

            if (arr[index] >= start){
                top = start;
            }

            while (++cursor < index){
                capacity += (top - arr[cursor]);
            }

        }

        return capacity;
    }

    /**
     * Given an array, arrange the elements such that the number formed by concatenating the elements is highest.
     * @param arr
     * @return
     */
    public List<String> getMaxNum(int[] arr) {
        boolean swapped = true;
        List<String> arr_String = Arrays.stream(arr).boxed().map(Object::toString).collect(Collectors.toList());
        while (swapped) {
            swapped = false;
            for (int i=0; i<arr_String.size()-1; i++) {
                String normal = arr_String.get(i) + arr_String.get(i+1);
                String reverse = arr_String.get(i+1) + arr_String.get(i);
                if (Integer.parseInt(normal) < Integer.parseInt(reverse)) {
                    Collections.swap(arr_String, i, i+1);
                    swapped = true;
                }
            }
            System.out.println(arr_String);
        }
        return arr_String;
    }

    /**
     * Given an array,find the maximum j – i such that arr[j] > arr[i]
     * @param arr
     * @return
     */
    public long getMaxDiff(int[] arr){
        long maxDiffRange = 0;
        boolean maxFound = false;
        for (int i=0; i<arr.length; i++){
            for (int j=arr.length-1; j>i; j--){
                if (arr[j] > arr[i]){
                    System.out.println(arr[i] + " : " + arr[j]);
                    maxDiffRange = j-i;
                    maxFound = true;
                    break;
                }
            }
            if (maxFound){
                break;
            }
        }
        return maxDiffRange;
    }

    /**
     *
     * @param states
     * @param days
     * @return
     */
    public List<Integer> cellCompete(int[] states, int days) {
        System.out.println("[" + Arrays.stream(states).boxed()
                .map(Object::toString)
                .collect(Collectors.joining(", ")) + "]");
        int[] tempStates = new int[states.length];
        for (int j=1; j<=days; j++) {
            for (int i = 0; i<states.length; i++) {
                int left = 0, right = 0;
                if (i == 0) {
                    right = states[i+1];
                }
                else if (i == states.length-1) {
                    left = states[i-1];
                }
                else {
                    left = states[i-1];
                    right = states[i+1];
                }
                tempStates[i] = left == right ? 0 : 1;
            }
            states = Arrays.copyOfRange(tempStates, 0, 8);
            System.out.println("[" + Arrays.stream(states).boxed()
                    .map(Object::toString)
                    .collect(Collectors.joining(", ")) + "]");
        }
        return Arrays.stream(states).boxed().collect(Collectors.toList());
    }

}
