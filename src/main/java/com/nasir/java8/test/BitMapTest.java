package com.nasir.java8.test;

import java.util.Arrays;
import java.util.List;

/**
 * Created by nasir on 26/10/18.
 */
public class BitMapTest {

    public static void main(String[] args) {
//        List<Short> bitNums = new ArrayList<>();
//        for(String bitNum : args) {
//            bitNums.add(new Short(bitNum));
//        }
//        getBitMap(bitNums);

        try {
            Arrays.asList("ACT", "NSW", "NT", "XXX", "X1201", "X34").forEach(state -> {
                int map = StateEnum.valueOf(state).getMap();
                System.out.println(map + " : " + ((map & 0) == 0));
            });
        } catch (IllegalArgumentException ex) {
            System.out.println("Exception -> " + StateEnum.XXX);
        }
    }

    public static Integer getBitMap(List<Short> bitNums) {
        int bitMap = 0;
        for(short bitNum : bitNums) {
            bitMap += (1 << bitNum);
        }
        System.out.println(bitMap);
        return bitMap;
    }

    public enum StateEnum {
        ACT (1<<0), AUS (1<<1),
        NSW (1<<2), NT (1<<3),
        QLD (1<<4), SA (1<<5),
        TAS (1<<6), VIC (1<<7), WA (1<<8),

        XXX (1<<9),

        X1201 (1<<10);

        private final int map;
        private StateEnum(int map) {
            this.map = map;
        }
        public int getMap () { return this.map; }
    }



}
