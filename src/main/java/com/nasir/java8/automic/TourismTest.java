package com.nasir.java8.automic;

import java.util.ArrayList;
import java.util.List;

public class TourismTest {

    public static void main(String[] args) {
        List<? extends Promotion> longWeekendPromo = new ArrayList(){{
            add(new OH3for2());
            add(new FreeSKwithOH());
            add(new BCFor20After4th());
        }};

//        TourismShopping tourism = new TourismShopping();
        TourismShopping tourism = new TourismShopping(longWeekendPromo);

        tourism.add(Tour.OH);
        tourism.add(Tour.OH);
        tourism.add(Tour.OH); // 600

        System.out.println(tourism.total());

        tourism.add(Tour.OH);
        tourism.add(Tour.SK); // 300

        System.out.println(tourism.total());

        tourism.add(Tour.OH);
        tourism.add(Tour.SK); // 300

        System.out.println(tourism.total());

        tourism.add(Tour.BC);
        tourism.add(Tour.BC);
        tourism.add(Tour.BC);
        tourism.add(Tour.BC);
        tourism.add(Tour.BC);
        tourism.add(Tour.BC); // 480

        System.out.println(tourism.total());


    }

}
