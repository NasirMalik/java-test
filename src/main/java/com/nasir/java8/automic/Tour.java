package com.nasir.java8.automic;

public enum Tour {

    OH ("Opera House", 300),
    BC ("Bridge Climb", 110),
    SK ("Sydney Sky Tower", 30);

    private String name;
    private Integer price;

    Tour(String name, Integer price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }
}
