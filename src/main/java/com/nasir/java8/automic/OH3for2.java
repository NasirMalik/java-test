package com.nasir.java8.automic;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;


/**
 * We are going to have a 3 for 2 deal on opera house ticket. For example, if you buy 3 tickets, you will pay the price of 2 only getting another one completely free of charge.
 */
public class OH3for2 implements Promotion {

    @Override
    public boolean isApplicable(List<Tour> tours) {
        Long count = tours.stream()
                .collect(Collectors.groupingBy(Tour::getName, Collectors.counting()))
                .get(Tour.OH.getName());
        return (count != null && count > 2);
    }

    @Override
    public int apply(Iterator<Tour> tours) {
        int promotionalPrice = Tour.OH.getPrice() * 2;
        int removed = 0;
        while (tours.hasNext() && removed < 3) {
            if (tours.next() == Tour.OH) {
                tours.remove();
                removed++;
            }
        }
        return promotionalPrice;
    }
}
