package com.nasir.java8.automic;

import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The Sydney Bridge Climb will have a bulk discount applied, where the price will drop to $20, if someone buys more than 4
 */
public class BCFor20After4th implements Promotion {

    @Override
    public boolean isApplicable(List<Tour> tours) {
        Long count = tours.stream()
                .collect(Collectors.groupingBy(Tour::getName, Collectors.counting()))
                .get(Tour.BC.getName());
        return (count != null && count > 4);
    }

    @Override
    public int apply(Iterator<Tour> tours) {
        int promotionalPrice = Tour.BC.getPrice() * 4;
        int removed = 0;
        while (tours.hasNext()) {
            if (tours.next() == Tour.BC) {
                tours.remove();
                removed++;
                if (removed > 4) {
                    promotionalPrice += 20;
                }
            }
        }
        return promotionalPrice;
    }
}
