package com.nasir.java8.automic;

import java.util.Iterator;
import java.util.List;

public interface Promotion {

    String PROMOTION_NAME = "";

    /**
     * To check the available tours for any propmotion to be applied
     * Must not alter the tour list
     *
     * @param tours
     * @return
     */
    boolean isApplicable(List<Tour> tours);

    /**
     * apply the promotion, should return this promotion cost and
     * should REMOVE the applied tours
     *
     * REMOVING is important
     *
     * @param tours
     * @return
     */
    int apply(Iterator<Tour> tours);

}
