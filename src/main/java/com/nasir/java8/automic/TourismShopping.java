package com.nasir.java8.automic;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class TourismShopping {

    private List<Tour> tours = new ArrayList<>();
    private List<? extends Promotion> promotions = new ArrayList<>();

    public TourismShopping(List<? extends Promotion> promotions) {
        this.promotions = promotions;
    }

    public void add(Tour t) {
        tours.add(t);
    }

    public int total() {
        // Cloning temporarily to a temp cart
        List<Tour> tempTours = new ArrayList<Tour>(tours.size());
        tours.forEach(tour -> tempTours.add(tour));

        int total = 0;

        Promotion promo = getApplicablePromo(tempTours);

        while (promo != null) {
            // if found a promotion applicable, apply and move on
            total += promo.apply(tempTours.iterator());
            promo = getApplicablePromo(tempTours);
        }

        total += tempTours.stream().collect(Collectors.summingInt(Tour::getPrice));
        tempTours.clear();

        return total;
    }

    /**
     * To check all the available promotions if anyone is applicable. First found is returned
     *
     * @param tours
     * @return
     */
    private Promotion getApplicablePromo(List<Tour> tours) {
        for (Promotion promo : promotions){
            if (promo.isApplicable(tours)) {
                return promo;
            }
        }
        return null;
    }

}




