package com.nasir.java8.automic;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * We are going to give a free Sky Tower tour for with every Opera House tour sold
 */
public class FreeSKwithOH implements Promotion {


    @Override
    public boolean isApplicable(List<Tour> tours) {
        Map<String, Long> map = tours.stream()
                .collect(Collectors.groupingBy(Tour::getName, Collectors.counting()));
        return ((map.get(Tour.OH.getName()) != null && map.get(Tour.OH.getName()) > 0)
                && (map.get(Tour.SK.getName()) != null && map.get(Tour.SK.getName()) > 0));
    }

    @Override
    public int apply(Iterator<Tour> tours) {
        int promotionalPrice = Tour.OH.getPrice();
        boolean removedOH = false, removedSK = false;
        while (tours.hasNext() && (!removedOH || !removedSK)) {
            Tour nextTour = tours.next();
            if (nextTour == Tour.OH && !removedOH) {
                tours.remove();
                removedOH = true;
            }
            if (nextTour == Tour.SK && !removedSK) {
                tours.remove();
                removedSK = true;
            }
        }
        return promotionalPrice;
    }
}
