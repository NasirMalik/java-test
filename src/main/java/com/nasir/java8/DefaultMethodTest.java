package com.nasir.java8;

interface Havedefault {

    String INTERFACE_ID = "TEST";

    default void defaultTest1() {
        System.out.println("defaultTest1 in interface called");
    }

    default void defaultTest2() {
        System.out.println("defaultTest2 in interface called");
    }

    static void staticTest1() {
        System.out.println("staticTest1 in interface called");
    }

    void implementMe();
}

class Implementation implements Havedefault {
    @Override
    public void implementMe() {
        System.out.println("implementMe in implementation called");
    }

    public void implementMe2(String text) {
        System.out.println(text);
    }

    public void implementMe3() {
        System.out.println("implementMe3 in implementation called");
    }
}

public class DefaultMethodTest {

    public static void main(String[] args) {
        Thread t1 = new Thread(Havedefault::staticTest1);
        t1.start();

        Implementation impl = new Implementation();
        Thread t2 = new Thread(impl::implementMe);
        t2.start();

        new Implementation().defaultTest1();
    }

}
