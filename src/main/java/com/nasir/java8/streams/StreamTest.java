package com.nasir.java8.streams;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class StreamTest {

    public static void main(String[] args) {
        StreamTest test = new StreamTest();
//        test.testIntermediary(Arrays.asList("subject1_1", "subject1_2", "subject1_3"));
//        test.timeIt();
//        test.timeIt2();
//        test.testStream();
        Arrays.asList("1", "2").forEach(StreamTest::testMethodRef);
    }

    private static void testMethodRef(String arg) {
        System.out.println(arg);
    }

    private void testStream() {
        Boolean[] cohorts = new Boolean[] {false, true, false, true, false, false, false};
        String log = "ACU applicant falls under cohorts -> " + IntStream.range(0, cohorts.length)
                .filter(i -> cohorts[i])
                .mapToObj(i -> String.valueOf(i+1))
                .collect(Collectors.joining(","));
        System.out.println(log);
    }

    private void testIntermediary(List<String> subject1) {
        List<String> subject2 = new ArrayList<String>(){{ add("subject2_1"); add("subject2_2"); }};

        subject1
                .stream()
                .peek(System.out::println)
                .filter(s -> s.length() > 2)
                .forEach(item -> subject2.add(item));

        System.out.println();
        subject2.forEach(System.out::println);
    }

    private void timeIt() {
        List<Long> longList = new ArrayList<>(1000-000);
        List<Long> longList2 = new ArrayList<>(1000-000);
        for (long i=0; i<1000-000; i++) {
            longList.add(i);
        }

        long start = System.currentTimeMillis();
        longList.forEach(listItem -> {
            if (listItem % 2 == 0) {
                longList2.add(listItem += listItem);
            }
        });
        long end = System.currentTimeMillis();

        longList2.subList(0, 10).forEach(System.out::println);
        System.out.println((end-start));
    }

    private void timeIt2() {
        List<Long> longList = new ArrayList<>(1000_000);
        List<Long> longList2;

        for (long i=0; i<1000_000; i++) {
            longList.add(i);
        }

        long start = System.currentTimeMillis();

        longList2 = longList.stream()
                .filter(listItem -> listItem % 2 == 0)
                .map(listItem -> listItem + listItem)
                .collect(Collectors.toList());
        long end = System.currentTimeMillis();

        longList2.subList(0, 10).forEach(System.out::println);
        System.out.println((end-start));
    }

}
