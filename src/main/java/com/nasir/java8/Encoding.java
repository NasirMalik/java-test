package com.nasir.java8;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

public class Encoding {

    public static void main(String[] args) throws NoSuchPaddingException, InvalidKeyException, DecoderException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeySpecException {
        System.out.println(decode("VW5RNMtLk6c5bAL3usOHNm8aQP3T2gDg1P1OtiP2uog7c1+XD0dgFOeasfXo1xss"));
    }

    public static String decode(String encodedStr) throws DecoderException, InvalidKeySpecException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        String password = "Secret Passphrase";
        String salt = "4acfedc7dc72a9003a0dd721d7642bde";
        String iv = "69135769514102d0eded589ff874cacd";
        byte[] saltBytes = Hex.decodeHex(salt.toCharArray());
        byte[] ivBytes = Hex.decodeHex(iv.toCharArray());
        IvParameterSpec ivParameterSpec = new IvParameterSpec(ivBytes);

        PBEKeySpec keySpec = new PBEKeySpec(password.toCharArray(), saltBytes, 100, 128);
        SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
        SecretKey secretKey = keyFactory.generateSecret(keySpec);
        SecretKeySpec sKey = new SecretKeySpec(secretKey.getEncoded(), "AES");

        Cipher c = Cipher.getInstance("AES/CBC/PKCS5Padding");
        c.init(Cipher.DECRYPT_MODE, sKey, ivParameterSpec);
        byte[] encodedVal = Base64.getDecoder().decode(encodedStr);
        byte[] valueBytes = c.doFinal(encodedVal);
        return new String(valueBytes);
    }

}
