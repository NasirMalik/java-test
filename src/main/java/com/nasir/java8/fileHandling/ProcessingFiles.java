package com.nasir.java8.fileHandling;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

public class ProcessingFiles {

    public static void main(String[] args) {

//        readConsoleInput();

        readFilesWithStream();
        readFilesWithReader();
        readFileInputWithScanner();
        readFileInputWithFiles();

    }

    private static void readFileInputWithFiles() {
        try {
            Files.readAllLines(Paths.get("input.txt"))
                    .stream()
//                    .skip(1)
                    .forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readFilesWithStream() {
        try (BufferedInputStream inputStream = new BufferedInputStream(new FileInputStream("input.txt"))) {
            int in;
            while ((in = inputStream.read()) != -1) {
                System.out.print((char)in);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readFilesWithReader() {
//        File file = new File("input.txt");
        try (BufferedReader reader = new BufferedReader(new FileReader("input.txt"))) {
            String text = "";
            while ((text = reader.readLine()) != null) {
                System.out.println(text);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void readFileInputWithScanner() {
        File file = new File("input.txt");
        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void readConsoleInput() {
        Scanner input = new Scanner(System.in);

        for (int i = 0; i < 3; i++) {
            System.out.print("Print something : ");
            String enteredText = input.nextLine();
            System.out.println("You printed : " + enteredText);
        }

        input.close();
    }

}
