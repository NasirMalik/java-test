package com.nasir.java8.concurrency;

public class TestThreading {

    public static void main(String args[])
    {
        final MathClass mathClass = new MathClass();

        //first thread
        Runnable r = () ->
            {
                try {
                    mathClass.printNumbers((int)(Math.random() * 100));
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            };

        new Thread(r, "ONE").start();
        new Thread(r, "TWO").start();
    }

}

class MathClass {

    private int counter = 0;

    void printNumbers(int n) throws InterruptedException
    {
//        synchronized (new Object())
//        {
            for (int i = 1; i <= n; i++)
            {
                synchronized (this) {
                    System.out.println(Thread.currentThread().getName() + " :: "+  i + " :: " + ++counter);
                }
                Thread.sleep(100);
            }
//        }
        System.out.println("\n" + Thread.currentThread().getName() + " -> " + n + "\n");
    }
}
