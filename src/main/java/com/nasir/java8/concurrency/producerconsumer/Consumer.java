package com.nasir.java8.concurrency.producerconsumer;

import java.util.List;

public class Consumer implements Runnable {

    List<Integer> questions = null;

    public Consumer(List<Integer> questions) {
        this.questions = questions;
    }

    public void answerQuestion() throws InterruptedException {
        synchronized (questions) {
            while (questions.isEmpty()) {
                System.out.println("Nothing, wait for questions");
                questions.wait();
            }
        }

        synchronized (questions) {
            Thread.sleep(5000);
            System.out.println("Answer to the question : " + questions.remove(0));
            questions.notify();
        }
    }

    @Override
    public void run() {
        while (true) {
            try {
                answerQuestion();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
