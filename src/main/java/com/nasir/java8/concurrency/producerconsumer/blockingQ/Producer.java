package com.nasir.java8.concurrency.producerconsumer.blockingQ;

import java.util.concurrent.BlockingQueue;

public class Producer implements Runnable {

    BlockingQueue<Integer> questions;
    private int questionNo;

    public Producer(BlockingQueue<Integer> questions) {
        this.questions = questions;
    }

    public void readQuestion(int questionNo) throws InterruptedException {
        System.out.println("New Question : " + questionNo);
        questions.put(questionNo);
        Thread.sleep(200);
    }

    @Override
    public void run() {
        while (questionNo <= 10) {
            try {
                readQuestion(questionNo++);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
