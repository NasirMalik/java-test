package com.nasir.java8.concurrency.producerconsumer.blockingQ;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable {

    BlockingQueue<Integer> questions = null;

    public Consumer(BlockingQueue<Integer> questions) {
        this.questions = questions;
    }

    public void answerQuestion() throws InterruptedException {
            Thread.sleep(400);
            System.out.println("Answer to the question : " + questions.take());
    }

    @Override
    public void run() {
        while (true) {
            try {
                answerQuestion();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
