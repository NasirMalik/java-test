package com.nasir.java8.concurrency.producerconsumer.blockingQ;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class Application {

    public static void main(String[] args) {
        BlockingQueue<Integer> questions = new ArrayBlockingQueue<Integer>(5);

        Thread producer = new Thread(new Producer(questions));
        Thread consumer = new Thread(new Consumer(questions));

        producer.start();
        consumer.start();
    }

}
