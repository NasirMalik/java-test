package com.nasir.java8.concurrency.producerconsumer;

import java.util.ArrayList;
import java.util.List;

public class Application {

    public static void main(String[] args) {
        List<Integer> questions = new ArrayList<>();

        Thread producer = new Thread(new Producer(questions));
        Thread consumer = new Thread(new Consumer(questions));

        producer.start();
        consumer.start();
    }

}
