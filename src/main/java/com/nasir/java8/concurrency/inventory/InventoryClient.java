package com.nasir.java8.concurrency.inventory;

public class InventoryClient {

    public static void main(String[] args) throws InterruptedException {
        InventoryManager manager = new InventoryManager();

        Thread inventoryTask = new Thread(() -> manager.populateSoldProducts());
        Thread printTask = new Thread(() -> manager.displaySoldProducts());

        inventoryTask.start();
//        inventoryTask.join();
        Thread.sleep(2000);
        printTask.start();
    }

}
