package com.nasir.java8.concurrency.inventory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

public class InventoryManager {

    /*
    Using CopyOnWriteArrayList instead of ArrayList which make possible the concurrent modification
     */
    List<Product> soldProducts = new CopyOnWriteArrayList<>();
    List<Product> purchasedProducts = new ArrayList<>();

    public void populateSoldProducts() {
        for (int i = 0; i < 1000; i++) {
            Product prod = new Product(i, "test_product_" + i);
            soldProducts.add(prod);
            System.out.println("Added : " + prod);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void displaySoldProducts() {
        soldProducts.forEach(prod -> {
            System.out.println("Printing the sold : " + prod);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
    }

}
