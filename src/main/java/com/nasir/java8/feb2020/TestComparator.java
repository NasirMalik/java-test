package com.nasir.java8.feb2020;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TestComparator {

    public static void main(String[] args) {

        List<String> names1 = new ArrayList<>();
        names1.add("Mahesh ");
        names1.add("Suresh ");
        names1.add("Ramesh ");
        names1.add("Naresh ");
        names1.add("Kalpesh ");

        List<String> names2 = new ArrayList<String>();
        names2.add("Mahesh ");
        names2.add("Suresh ");
        names2.add("Ramesh ");
        names2.add("Naresh ");
        names2.add("Kalpesh ");

        testJava7(names1);
        testJava8(names2);
    }

    public static void testJava7(List<String> list) {
         Collections.sort(list, new Comparator<String>() {
             @Override
             public int compare(String o1, String o2) {
                 return o1.compareTo(o2);
             }
         });
        System.out.println(list);
    }

    public static void testJava8(List<String> list) {
        Collections.sort(list, (o1, o2) ->  o1.compareTo(o2));
        System.out.println(list);
    }

}
