package com.nasir.java8.feb2020;

import java.util.Arrays;
import java.util.List;

@FunctionalInterface
interface BinaryOperation {
    int operate(int o1, int o2);
}

public class TestLambda {

    static int testOperation(BinaryOperation operation, int o1, int o2) {
        System.out.print(o1 + " : " + o2 + " --> ");
        return operation.operate(o1, o2);
    }

    public static void main(String[] args) {
        BinaryOperation add             = (int o1, int o2) -> o1+o2;
        BinaryOperation subtract        = (int o1, int o2) -> o1-o2;
        BinaryOperation product         = (int o1, int o2) -> o1*o2;
        BinaryOperation divide          = (int o1, int o2) -> o1/o2;

        List<BinaryOperation> operations = Arrays.asList(add, subtract, product, divide);

        operations.forEach(operation -> System.out.println(testOperation(operation, (int)(Math.random() * 10), (int)(Math.random() * 10))));
    }

}
