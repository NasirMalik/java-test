package com.nasir.java8.feb2020;

interface Vehicle {
    default void print() {
        System.out.println("I am a Vehicle");
    }

    static void blowHorn() {
        System.out.println("I am a Vehicle, and can blow a horn");
    }
}

interface FourWheeler {
    default void print() {
        System.out.println("I am a FourWheeler");
    }
}

class Car implements Vehicle, FourWheeler {
    public void print() {
        System.out.println("I am a Car");
        Vehicle.super.print();
        FourWheeler.super.print();

        Vehicle.blowHorn();
    }
}

public class TestDefaultMethods {

    public static void main(String[] args) {
        Car car = new Car();
        car.print();
    }

    public int testingStartsHere() {
        return 1;
    }
}
