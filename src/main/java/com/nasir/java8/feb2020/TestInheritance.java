package com.nasir.java8.feb2020;

interface BaseInterface {
    static void staticMethod() {
        System.out.println("I am in the staticMethod in BaseInterface");
    }

//    void instanceMethod();

//    default void instanceMethod() {
//        System.out.println("I am in the instanceMethod in BaseInterface");
//    }
}

class Base {
    static void staticMethod() {
        System.out.println("I am in the staticMethod in Base");
    }

    protected void instanceMethod() {
        System.out.println("I am in the instanceMethod in Base");
    }
}

class Derived extends Base implements BaseInterface {
    @Override
    protected void instanceMethod() {
        super.instanceMethod();
//        BaseInterface.super.instanceMethod();
        System.out.println("I am in the instanceMethod in Derived, required to implement one from interface");

        Base.staticMethod();
        BaseInterface.staticMethod();
    }

    public static void staticMethod() {
        System.out.println("I am in the staticMethod in Derived");
    }
}

public class TestInheritance {

    static int number2 = getValue();
    static int number1 = 10;
    static int getValue() {
        System.out.println(number1);
        return number1;
    }

    static int doMinus() { return number1 - number2; }
    static int doSum() { return number1 + number2; }

    TestInheritance() {
        super();
    }

    public static void main(String[] args) {

        new TestInheritance();

        Integer a = 1000, b= 1000;
        System.out.println(a == b);
        Integer k = 127, n= 127;
        System.out.println(k == n);

//        System.out.println(doSum());
//        System.out.println(doMinus());

//        test(args);
//        int a = 9, b =2;
//        float f = a/b;
//        System.out.println("f - > " + f);
//        f = f/2;
//        System.out.println("f - > " + f);
//        f = a+ b / f;
//        System.out.println("f - > " + f);
//
//        byte arr[] = new byte[]{1, 2, 3, 4};
//        for (final int i : arr ) {
//            System.out.print(i + " ");
//        }
    }

//    public void test(String[] args) {
//        System.out.println(args);
//        System.out.println(args[1]);
//    }
//        Derived derived = new Derived();
//        derived.staticMethod();
//        derived.instanceMethod();

//        Base base = new Base();
//        base.staticMethod();


//        YearMonth ym1 = YearMonth.of(2016, Month.FEBRUARY);
//        YearMonth ym2 = YearMonth.now();
//
//        System.out.println(ym1.minus(Period.ofMonths(4)).getMonthValue());
//
//        System.out.println(String.valueOf(1));
//
//        Sugesstion sugges = new Sugesstion() {
//            public void method2() {
//                method1();
//            }
//
//            public void method1() {
//                System.out.println("testing");
//            }
//
//
//        };
//
//        sugges.method2();
//    }

}

class Sugesstion {
    private void method1() {

    }

    public void method2() {

    }
}

//interface Account {
//    BigDecimal dec = new BigDecimal(0.00);
//}
//
//class SavingsAccount implements Account {
//    public SavingsAccount(BigDecimal init) {
//        dec = init;
//    }
//}
