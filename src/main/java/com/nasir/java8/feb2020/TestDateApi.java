package com.nasir.java8.feb2020;

import java.time.Duration;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;

public class TestDateApi {

    public static void main(String[] args) {
        LocalTime now = LocalTime.now();

        System.out.println(now);

        System.out.println(Duration.between(now.plus(1, ChronoUnit.HOURS), now));
    }

}
