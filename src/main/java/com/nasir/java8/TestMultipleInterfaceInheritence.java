package com.nasir.java8;

public class TestMultipleInterfaceInheritence {

    public static void main(String[] args) {
        DerivedClass d = new DerivedClass();
        d.defaultMethod();
        d.abstractMethod();
//        System.out.println(d.NAME);
        System.out.println(d.myName);
        d.staticMethod();
//        Contract1.staticMethod();
//        Contract2.staticMethod();
        System.out.println(Contract1.NAME);

        System.out.println("");
        DerivedClass2 d2 = new DerivedClass2();
        d2.defaultMethod();
        d2.abstractMethod();
        System.out.println(d2.myName);
        d2.staticMethod();
    }

}

interface Contract1 {
    static String NAME = "Contract1";

    default void defaultMethod() {
        System.out.println("In Contract1 -> defaultMethod");
    }

    static void staticMethod() {
        System.out.println("In Contract1 -> staticMethod");
    }

    void abstractMethod();
}

interface Contract2 {
    static String NAME = "Contract2";

    default void defaultMethod() {
        System.out.println("In Contract1 -> defaultMethod");
    }

    static void staticMethod() {
        System.out.println("In Contract2 -> staticMethod");
    }

    void abstractMethod();
}

class DerivedClass implements Contract1, Contract2 {
    static String myName = "DerivedClass";

    @Override
    public void defaultMethod() {
        Contract1.super.defaultMethod();
        Contract2.super.defaultMethod();
    }

    @Override
    public void abstractMethod() {
        System.out.println("Testing Multiple");
    }

    static void staticMethod() {
        System.out.println("In DerivedClass -> staticMethod");
    }
}

class DerivedClass2 extends DerivedClass {
    String myName = "DerivedClass2";
}
