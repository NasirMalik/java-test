package com.nasir.java8.oo;

public class Earth {

    public static void main(String... args) {
        Human nasir = new Human("Nasir", 37, 68, "Brown");
        nasir.speak();
        nasir.eat();
        nasir.walk();
        nasir.work();

        Human zombie = new Human();
        zombie.speak();
        zombie.eat();
        zombie.walk();
        zombie.work();
    }

}
