package com.nasir.java8.oo;

public class Human {

    private String name;
    private int age;
    private int heightInInches;
    private String eyeColor;

    public Human() {
    }

    public Human(String name, int age, int heightInInches, String eyeColor) {
        this.name = name;
        this.age = age;
        this.heightInInches = heightInInches;
        this.eyeColor = eyeColor;
    }

    public void speak() {
        System.out.println("Hello, I am speaking. I am " + this);
    }

    public void eat() {
        System.out.println("Hello, I am eating");
    }

    public void walk() {
        System.out.println("Hello, I am walking");
    }

    public void work() {
        System.out.println("Hello, I am working");
    }

    @Override
    public String toString() {
        return "Human {" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", heightInInches=" + heightInInches +
                ", eyeColor='" + eyeColor + '\'' +
                '}';
    }
}
