package com.nasir.java8;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

public class ListImmutability {

    static Logger log = Logger.getLogger("ListImmutability");

    public static void main(String[] args) {
        ListImmutability obj = new ListImmutability();
        List<String> stringList1 = obj.getList();
        stringList1.add("Test from outside");
        obj.checkList(stringList1);
        log.info("in main : " + stringList1);
    }

    private void checkList(List<String> stringList1) {
        log.info("in checkList : " + stringList1);
        stringList1.add("Test from checkList");
    }


    private List<String> getList(){
//        return new ArrayList<String>() {{ add("Test from inside"); }};
        return Collections.emptyList();
//        return Arrays.asList();
//        return new ArrayList<>();
    }

}
